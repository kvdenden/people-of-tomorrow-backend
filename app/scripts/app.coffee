"use strict"

app = angular.module('pot', ["ngCookies", "ngResource", "ngSanitize", "ngRoute", "ui.bootstrap"])

angular.module("template/pagination/pagination.html", []).run ["$templateCache", ($templateCache) ->
  $templateCache.put "template/pagination/pagination.html", "<div class=\"pagination-wrap\"><ul class=\"pagination pagination-sm\">\n" + "  <li ng-repeat=\"page in pages\" ng-class=\"{active: page.active, disabled: page.disabled}\"><a ng-click=\"selectPage(page.number)\">{{page.text}}</a></li>\n" + "  </ul>\n" + "</div>\n" + ""
]

app.factory "Authorize", ['$http', '$location', ($http, $location) ->
  allow = (roles...) ->
    success = (data, status) ->
      if 'roles' of data
        user_roles = data.roles.split(",")
        if _.intersection(roles, user_roles).length is 0
          console.log "Not authorized to access this page"
          $location.path("/")
      else
        console.log "Please log in first"
        $location.path("/login")
    error = (data, status) ->
      console.log "Not authorized to access this page"
      $location.path("/login")
    $http.get("api/api.php/users/current").success(success).error(error)
]

app.config ($routeProvider) ->
  $routeProvider
	# .when "/admin", # user management
	# 	templateUrl: "views/admin.html"
	# 	controller: "AdminCtrl"

  .when "/main",
    templateUrl: "views/main.html"
    controller: "MainCtrl"

  .when "/login",
    templateUrl: "views/login.html"
    controller: "LoginCtrl"

  .when "/logout",
    templateUrl: "views/logout.html"
    controller: "LogoutCtrl"

  .when "/users",
    templateUrl: "views/users.html"
    controller: "UsersCtrl"

  .when "/orders", # show list of orders
  	templateUrl: "views/orders.html"
  	controller: "OrdersCtrl"

  .when "/orders/:id", # order detail
  	templateUrl: "views/view_order.html"
  	controller: "ViewOrderCtrl"

  .when "/orders/:id/edit", # edit order
    templateUrl: "views/edit_order.html"
    controller: "EditOrderCtrl"

  .when "/credits", # show list of credits
  	templateUrl: "views/credits.html"
  	controller: "CreditsCtrl"

  .when "/designs", # show list of designs
  	templateUrl: "views/designs.html"
  	controller: "DesignsCtrl"

  .when "/validate", # show list of designs
    templateUrl: "views/validations.html"
    controller: "ValidationsCtrl"

  .when "/validate/random", # design detail
    templateUrl: "views/view_validation.html"
    controller: "RandomValidationCtrl"
  
  .when "/validate/:id", # design detail
    templateUrl: "views/view_validation.html"
    controller: "ViewValidationCtrl"

 #  .when "/designs/:id", # design details
 #  	templateUrl: "views/designdetail.html"
 #  	controller: "DesignDetailCtrl"

  .when "/neighbours", # show list of neighbours
  	templateUrl: "views/neighbours.html"
  	controller: "NeighboursCtrl"

  .when "/exports", # show list of exports
    templateUrl: "views/exports.html"
    controller: "ExportsCtrl"

  .when "/winners", # extra for people of the schorre
    templateUrl: "views/winners.html"
    controller: "WinnersCtrl"

  .otherwise redirectTo: "/main"


