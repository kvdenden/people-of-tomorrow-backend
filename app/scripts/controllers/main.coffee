class MainCtrl
	constructor: ($scope, $http, $location) ->
		window.scope = $scope
		console.log("got here")
		$http.get("api/api.php/users/current").success (data, status) ->
			console.log(data)
			if 'roles' of data
				user_roles = data.roles.split(",")
				if _.contains user_roles, 'admin'
					$location.path("/orders")
				else if _.contains user_roles, 'helpdesk'
					$location.path("/orders")
				else if _.contains user_roles, 'validator'
					$location.path("/validate")
			else
				$location.path("/login")
      
angular.module("pot").controller("MainCtrl", ["$scope", "$http", "$location", MainCtrl])