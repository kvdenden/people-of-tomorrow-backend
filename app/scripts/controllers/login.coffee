class LoginCtrl
	constructor: ($scope, $http, $location) ->
		$scope.login = () ->
			console.log("logging in...")
			success = (data, status) ->
				$location.path("/")
			error = (data, status) ->
				alert("Invalid credentials")
			$http.post("api/api.php/login", {username: $scope.username, password_hash: md5($scope.password)}).success(success).error(error)

angular.module("pot").controller("LoginCtrl", ["$scope", "$http", "$location", LoginCtrl])