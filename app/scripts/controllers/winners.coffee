class Winner
	constructor: (data) ->
		@data = data # all data

		@id = data.id
		date = moment(data.transaction_date)

		filename = if data.filename then "<a href='http://peopleoftomorrow.com/preview/#{data.filename}'>#{data.filename}</a>" else null

		@attributes =
			transaction_date: if date.isValid() then date.format("ll") else null
			flow: data.flow
			cardholder: data.cardholder
			email: data.email
			design: filename

	get: (key) ->
		if key of @attributes
			@attributes[key]

class WinnersCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.winners = []

		$scope.columns = [
			title: "Date"
			key: "transaction_date"
		,
			title: "Flow"
			key: "flow"
		,
			title: "Name"
			key: "cardholder"
		,
			title: "Email"
			key: "email"
		,
			title: "Design"
			key: "design"
		]
		
		$scope.options =
			filters:
				flow: ""

		$scope.loadWinnersWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadWinners(options)
			), delay

		$scope.loadWinners = (options) ->

			$scope.loading = true

			options ?= $scope.options

			query = "flow=#{options.filters.flow}"

			success = (data, status) ->
				$scope.loading = false
				$scope.winners = _.map(data, (d) -> new Winner(d))
				$scope.$apply() unless $scope.$$phase

				# setTimeout ( ->
				# 	$(".nailthumb-container").nailthumb()
				# ), 100

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)
			$http.get("api/api.php/winners?#{query}&_=#{new Date().getTime()}").success(success).error(error)

		# $scope.csvLink = (options) ->
		# 	options ?= $scope.options

		# 	from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
		# 	to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

		# 	query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
		# 	query += "&from=#{from_date}" if from_date?
		# 	query += "&to=#{to_date}" if to_date?
		# 	query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
		# 	query += "&search=#{options.search}" unless _.string.isBlank(options.search)
		# 	query += "&with_designs=#{options.filters.with_designs}" unless _.string.isBlank(options.filters.with_designs)
		# 	query += "&csv=1"

		# 	"api/api.php/credits?#{query}"

		$(".form-control.filter").change () ->
			$scope.loadWinnersWithDelay($scope.options) # hack, why doesn't $watch work???

		$scope.loadWinners($scope.options)

		# $("input.date").datepicker()

		window.scope = $scope

angular.module("pot").controller("WinnersCtrl", ["$scope", "$http", "Authorize", WinnersCtrl])