class Neighbour
	constructor: (data) ->
		@data = data # all data

		date = moment(data.transaction_date)

		@id = data.id

		filename = if data.filename then "http://peopleoftomorrow.com/preview/#{data.filename}" else null

		@attributes =
			# transaction_date: if date.isValid() then date.format("ll") else null
			# cardholder: data.cardholder
			# email: data.email
			filename: if data.filename then "<div class='thumb-wrapper'><a class='nailthumb-container square-thumb' href='#{filename}' target='_blank'><img src='#{filename}' /></a></div>" else ""
			token: data.token
			firstname: "#{data.firstname} #{data.lastname}"
			email: data.email
			street: "#{data.street} #{data.streetnr}, #{data.zip} #{data.city}"
			country: data.country

	get: (key) ->
		if key of @attributes
			@attributes[key]

class NeighboursCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.neighbours = []

		$scope.total_neighbours = 0

		$scope.columns = [
			title: "Token"
			key: "token"
			sortable: true
		,
			title: "Name"
			key: "firstname"
			sortable: true
		,
			title: "Email"
			key: "email"
			sortable: true
		,
			title: "Address"
			key: "street"
			sortable: true
		,
			title: "Country"
			key: "country"
			sortable: true
		,
			title: "Thumbnail"
			key: "filename"
			sortable: false
		]
		
		$scope.options =
			page: 1
			items_per_page: 25
			sort_by: "neighbours.id"
			sort_how: "DESC"
			# filters:
			# 	from_date: ""
			# 	to_date: ""
			# 	type: ""
			search: ""

		# $scope.filters =
		# 	type: [
		# 		title: "Show all types"
		# 		key: ""
		# 	,
		# 		title: "Only show normal"
		# 		key: "normal"
		# 	,
		# 		title: "Only show large"
		# 		key: "large"
		# 	,
		# 		title: "Only show group"
		# 		key: "group"
		# 	]

		$scope.clearNeighbour = (id) ->
			console.log("clearing neighbour #{id}")
			success = (data, status) ->
				$scope.loadNeighbours($scope.options)
			error = (data, status) ->
				console.log("error")
				console.log(data)
			$http.put("api/api.php/neighbours/#{id}").success(success).error(error)

		$scope.sendEmail = (id) ->
			$http.post("api/api.php/neighbours/#{id}/email").success () ->
				alert("email sent")

		$scope.loadNeighboursWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadNeighbours(options)
			), delay

		$scope.loadNeighbours = (options) ->

			$scope.loading = true

			options ?= $scope.options

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)

			success = (data, status) ->
				$scope.loading = false
				$scope.neighbours = _.map(data, (d) -> new Neighbour(d))
				$scope.$apply() unless $scope.$$phase

				setTimeout ( ->
					$(".nailthumb-container").nailthumb()
				), 100

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)
			$http.get("api/api.php/neighbours?#{query}&_=#{new Date().getTime()}").success(success).error(error)
			$http.get("api/api.php/neighbours?#{query}&total=1&_=#{new Date().getTime()}").success (data, status) -> $scope.total_neighbours = data.total

		$scope.sort = (column) ->
			if _.findWhere($scope.columns, {key: column})
				if $scope.options.sort_by is column
					# reverse sort order
					$scope.options.sort_how = if $scope.options.sort_how is "DESC" then "ASC" else "DESC"
				else
					$scope.options.sort_by = column
					$scope.options.sort_how = "ASC"

				$scope.loadNeighbours($scope.options)

		$scope.csvLink = (options) ->
			options ?= $scope.options

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&csv=1"

			"api/api.php/neighbours?#{query}"

		$(".form-control.filter").change () ->
			$scope.options.page = 1
			$scope.loadNeighboursWithDelay($scope.options) # hack, why doesn't $watch work???

		$scope.loadNeighbours($scope.options)

		# $("input.date").datepicker()

		window.scope = $scope

angular.module("pot").controller("NeighboursCtrl", ["$scope", "$http", "Authorize", NeighboursCtrl])