class Design
	constructor: (data) ->
		@data = data # all data

		@id = data.id

		# date = moment(data.transaction_date)
		filename = "http://peopleoftomorrow.com/preview/#{data.filename}"

		@attributes =
			# transaction_date: if date.isValid() then date.format("ll") else null
			# cardholder: data.cardholder
			# email: data.email
			width: data.width
			height: data.height
			type: if data.type then data.type else "<span class='text-danger'>no credit</span>"
			token: data.token
			group_id: if data.group_id is "0" then "" else data.group_id
			filename: "<div class='thumb-wrapper'><a class='nailthumb-container square-thumb' href='#{filename}' target='_blank'><img src='#{filename}' /></a></div>"

	get: (key) ->
		if key of @attributes
			@attributes[key]

class DesignsCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.designs = []

		$scope.total_designs = 0

		$scope.columns = [
			title: "Thumbnail"
			key: "filename"
			sortable: false
		,
			title: "Type"
			key: "type"
			sortable: true
		,
			title: "Code"
			key: "token"
			sortable: true
		,
			title: "Group ID"
			key: "group_id"
			sortable: true
		]
		
		$scope.options =
			page: 1
			items_per_page: 25
			sort_by: "designs.id"
			sort_how: "DESC"
			filters:
				from_date: ""
				to_date: ""
				type: ""
				with_credits: ""
			search: ""

		$scope.filters =
			type: [
				title: "Show all types"
				key: ""
			,
				title: "Only show normal"
				key: "normal"
			,
				title: "Only show large"
				key: "large"
			,
				title: "Only show group"
				key: "group"
			]
			with_credits: [
				title: "With and without credit"
				key: ""
			,
				title: "Only with credit"
				key: "1"
			,
				title: "Only without credit"
				key: "0"
			]

		$scope.deleteDesign = (id) ->
			success = (data, status) ->
				alert("Design deleted")
				$scope.loadDesigns($scope.options)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/designs/#{id}").success(success).error(error)

		$scope.loadDesignsWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadDesigns(options)
			), delay

		$scope.loadDesigns = (options) ->

			$scope.loading = true

			options ?= $scope.options

			# from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			# to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			# query += "&from=#{from_date}" if from_date?
			# query += "&to=#{to_date}" if to_date?
			query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&with_credits=#{options.filters.with_credits}" unless _.string.isBlank(options.filters.with_credits)

			success = (data, status) ->
				$scope.loading = false
				$scope.designs = _.map(data, (d) -> new Design(d))
				$scope.$apply() unless $scope.$$phase

				setTimeout ( ->
					$(".nailthumb-container").nailthumb()
				), 100

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)
			$http.get("api/api.php/designs?#{query}&_=#{new Date().getTime()}").success(success).error(error)
			$http.get("api/api.php/designs?#{query}&total=1&_=#{new Date().getTime()}").success (data, status) -> $scope.total_designs = data.total

		$scope.sort = (column) ->
			if _.findWhere($scope.columns, {key: column})
				if $scope.options.sort_by is column
					# reverse sort order
					$scope.options.sort_how = if $scope.options.sort_how is "DESC" then "ASC" else "DESC"
				else
					$scope.options.sort_by = column
					$scope.options.sort_how = "ASC"

				$scope.loadDesigns($scope.options)

		$scope.csvLink = (options) ->
			options ?= $scope.options

			# from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			# to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			# query += "&from=#{from_date}" if from_date?
			# query += "&to=#{to_date}" if to_date?
			query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&with_credits=#{options.filters.with_credits}" unless _.string.isBlank(options.filters.with_credits)
			query += "&csv=1"

			"api/api.php/designs?#{query}"

		$(".form-control.filter").change () ->
			$scope.options.page = 1
			$scope.loadDesignsWithDelay($scope.options) # hack, why doesn't $watch work???

		$scope.loadDesigns($scope.options)

		window.scope = $scope

angular.module("pot").controller("DesignsCtrl", ["$scope", "$http", "Authorize", DesignsCtrl])