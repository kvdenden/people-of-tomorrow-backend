class LogoutCtrl
	constructor: ($scope, $http, $location) ->
		$http.get("api/api.php/logout").success () ->
			$location.path("/")

angular.module("pot").controller("LogoutCtrl", ["$scope", "$http", "$location", LogoutCtrl])