class Export
	constructor: (data) ->
		@data = data # all data

		@id = data.id
		
		date = moment(data.creation_date)

		filename = if data.filename then window.location.origin + "/exports/#{data.filename}" else null

		@attributes =
			creation_date: if date.isValid() then date.format("ll") else null
			filename: if data.filename then "<a class='btn btn-link' href='#{filename}' target='_blank'>#{filename}</a>" else ""

	get: (key) ->
		if key of @attributes
			@attributes[key]

class ExportsCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.exports = []

		$scope.columns = [
			title: "Date"
			key: "creation_date"
			sortable: true
		,
			title: "Filename"
			key: "filename"
			sortable: false
		]
		
		$scope.options =
			sort_by: "creation_date"
			sort_how: "DESC"

		$scope.amount = 100

		$scope.createExport = () ->
			success = (data, status) ->
				$scope.loadExports($scope.options)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.post("api/api.php/exports", {"limit": $scope.amount}).success(success).error(error)

		$scope.deleteExport = (id) ->
			success = (data, status) ->
				alert("Export deleted")
				$scope.loadExports($scope.options)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/exports/#{id}").success(success).error(error)

		$scope.loadExportsWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadExports(options)
			), delay

		$scope.loadExports = (options) ->

			$scope.loading = true

			options ?= $scope.options

			query = "sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"

			success = (data, status) ->
				$scope.loading = false
				$scope.exports = _.map(data, (d) -> new Export(d))
				$scope.$apply() unless $scope.$$phase

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)
			$http.get("api/api.php/exports?#{query}&_=#{new Date().getTime()}").success(success).error(error)

		$scope.sort = (column) ->
			if _.findWhere($scope.columns, {key: column})
				if $scope.options.sort_by is column
					# reverse sort order
					$scope.options.sort_how = if $scope.options.sort_how is "DESC" then "ASC" else "DESC"
				else
					$scope.options.sort_by = column
					$scope.options.sort_how = "ASC"

				$scope.loadExports($scope.options)

		$scope.loadExports($scope.options)

		window.scope = $scope

angular.module("pot").controller("ExportsCtrl", ["$scope", "$http", "Authorize", ExportsCtrl])