class NavCtrl
	constructor: ($scope, $http, $location) ->
		$scope.logged_in = false
		$scope.roles = []

		$scope.loadCurrentUser = () ->
			$http.get("api/api.php/users/current?_=#{new Date().getTime()}").success (data, status) ->
				if 'id' of data
					$scope.logged_in = true
					$scope.roles = data.roles.split(",")
				else
					$scope.logged_in = false
					$scope.roles = []

		$scope.is = (role) ->
			_.contains $scope.roles, role

		$scope.loadCurrentUser()

		$scope.$on '$locationChangeSuccess', (event, from, to) ->
			$scope.loadCurrentUser()
			setTimeout ( ->
				$(".nav li.active").removeClass('active')
				$(".nav a[href='"+window.location.hash+"']").parent("li").addClass('active')
			), 100

		window.scope = $scope

angular.module("pot").controller("NavCtrl", ["$scope", "$http", "$location", NavCtrl])