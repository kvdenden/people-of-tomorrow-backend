"use strict";

class Order
	constructor: (data) ->
		@data = data # all data

		@id = data.id

		date = moment(data.transaction_date)

		@attributes =
			order_id: data.order_id
			transaction_date: if date.isValid() then date.format("ll") else null
			cardholder: data.cardholder
			email: data.email
			amount: "#{data.amount / 100} #{data.currency}"
			paid: if data.paid is "1" then "\u2714" else ""
			number_of_credits: if data.number_of_credits is "0" then "<span class='text-danger'>no credits</span>" else "#{data.number_of_credits} credits" 

	get: (key) ->
		if key of @attributes
			@attributes[key]

class Credit
	constructor: (order, data) ->
		@order = order
		@data = data

		@id = data.id
		@design_id = data.design_id

		filename = if data.filename then "http://peopleoftomorrow.com/preview/#{data.filename}" else null

		@attributes =
			token: data.token
			type: data.type
			group_id: if data.group_id is "0" then "" else data.group_id
			design: if data.filename then "<div class='thumb-wrapper'><a class='nailthumb-container square-thumb' href='#{filename}' target='_blank'><img src='#{filename}' /></a></div>" else ""
			# design: data.design

	get: (key) ->
		if key of @attributes
			@attributes[key]


class OrdersCtrl
	constructor: ($scope, $http, $location, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.orders = []

		$scope.total_orders = 0

		$scope.new_order =
			cardholder: ''
			email: ''

		$scope.columns = [
			title: "Date"
			key: "transaction_date"
			sortable: true
		,
			title: "Order ID"
			key: "order_id"
			sortable: true
		,
			title: "Name"
			key: "cardholder"
			sortable: true
		,
			title: "Email"
			key: "email"
			sortable: true
		,
			title: "Amount"
			key: "amount"
			sortable: true
		,
			title: "Paid"
			key: "paid"
			sortable: true
		,
			title: "Number of credits"
			key: "number_of_credits"
			sortable: true
		]

		$scope.messages = []
		
		$scope.options =
			page: 1
			items_per_page: 25
			sort_by: "transaction_date"
			sort_how: "DESC"
			filters:
				from_date: ""
				to_date: ""
				paid: ""
			search: ""

		$scope.filters =
			paid: [
				title: "Show paid and unpaid"
				key: ""
			,
				title: "Only show paid"
				key: "1"
			,
				title: "Only show unpaid"
				key: "0"
			]

		$scope.loadOrdersWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadOrders(options)
			), delay

		$scope.deleteOrder = (id) ->
			$scope.loading = true
			success = (data, status) ->
				$scope.loadOrders($scope.options)
			error = (data, status) ->
				$scope.loading = false
				alert("Something went wrong...")
			$http.delete("api/api.php/orders/#{id}").success(success).error(error)

		$scope.saveNewOrder = () ->
			success = (data, status) ->
				$("#order-modal").modal('hide')
				if 'id' of data
					$("#order-modal").on 'hidden.bs.modal', () =>
						$location.path("/orders/#{data.id}")
						$scope.$apply() unless $scope.$$phase
				$scope.loadOrders($scope.options)
				# if 'id' of data
				# 	setTimeout ( ->
				# 		$location.path("/orders/#{data.id}")
				# 	), 100
				
			error = (data, status) ->
				alert("Something went wrong...")
			$http.post("api/api.php/orders", $scope.new_order).success(success).error(error)

		$scope.showNewOrderModal = () ->
			$scope.new_order.cardholder = ''
			$scope.new_order.email = ''

			$("#order-modal").modal('show')

		$scope.loadOrders = (options) ->

			$scope.loading = true

			options ?= $scope.options

			from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			query += "&from=#{from_date}" if from_date?
			query += "&to=#{to_date}" if to_date?
			query += "&paid=#{options.filters.paid}" unless _.string.isBlank(options.filters.paid)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)

			success = (data, status) ->
				$scope.loading = false
				$scope.orders = _.map(data, (d) -> new Order(d))
				$scope.$apply() unless $scope.$$phase

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log(data)
			$http.get("api/api.php/orders?#{query}&_=#{new Date().getTime()}").success(success).error(error)
			$http.get("api/api.php/orders?#{query}&total=1&_=#{new Date().getTime()}").success (data, status) -> $scope.total_orders = data.total

		$scope.sort = (column) ->
			if _.findWhere($scope.columns, {key: column})
				if $scope.options.sort_by is column
					# reverse sort order
					$scope.options.sort_how = if $scope.options.sort_how is "DESC" then "ASC" else "DESC"
				else
					$scope.options.sort_by = column
					$scope.options.sort_how = "ASC"

				$scope.loadOrders($scope.options)

		$scope.csvLink = (options) ->
			options ?= $scope.options

			from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			query += "&from=#{from_date}" if from_date?
			query += "&to=#{to_date}" if to_date?
			query += "&paid=#{options.filters.paid}" unless _.string.isBlank(options.filters.paid)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&csv=1"

			"api/api.php/orders?#{query}"

		$scope.orderSummary = (options) ->
			options ?= $scope.options

			from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = ""
			query += "&from=#{from_date}" if from_date?
			query += "&to=#{to_date}" if to_date?
			query += "&paid=#{options.filters.paid}" unless _.string.isBlank(options.filters.paid)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)

			"api/api.php/orders/summary?#{query}"

		# $scope.$watch 'options.filters.from_date', $scope.loadOrders($scope.options), true
		# $scope.$watch 'options.filters.to_date', $scope.loadOrders($scope.options), true
		# $scope.$watch (() -> $scope.options), $scope.loadOrders($scope.options), true

		$(".form-control.filter").change () ->
			$scope.options.page = 1
			$scope.loadOrdersWithDelay($scope.options) # hack, why doesn't $watch work???

		$scope.loadOrders($scope.options)

		$("input.date").datepicker()
		window.scope = $scope

class ViewOrderCtrl
	constructor: ($scope, $http, $location, $routeParams, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.credit_columns = [
			title: "Code"
			key: "token"
			sortable: true
		,
			title: "Type"
			key: "type"
			sortable: true
		,
			title: "Group ID"
			key: "group_id"
			sortable: true
		,
			title: "Design"
			key: "design"
			sortable: false
		]

		$scope.credit_types = [ 'normal', 'large', 'group' ]
		$scope.newcredit =
			type: $scope.credit_types[0]
			group_id: ''
			amount: 1

		$scope.generateCredit = (type, amount, group_id) ->
			$scope.loading = true
			# console.log("generate credit... with type #{type} and group id #{group_id}")
			# if type is 'group' and _.string.isBlank(group_id)
			# 	'' # generate group id on client?
				
			success = (data, status) ->
				$scope.loadCredits($scope.order.id)
			error = (data, status) ->
				$scope.loading = false
				alert("Something went wrong...")

			$http.post("api/api.php/orders/#{$scope.order.id}/credit", {type: type, group_id: group_id, amount: amount}).success(success).error(error)

		$scope.deleteCredit = (id) ->
			$scope.loading = true
			success = (data, status) ->
				$scope.loadCredits($scope.order.id)
			error = (data, status) ->
				$scope.loading = false
				alert("Something went wrong...")
			$http.delete("api/api.php/credits/#{id}").success(success).error(error)
		
		$scope.deleteDesign = (id) ->
			success = (data, status) ->
				$scope.loadCredits($scope.order.id)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/designs/#{id}").success(success).error(error)

		$scope.editOrder = (id) ->
			$location.path("/order/#{id}/edit")

		$scope.deleteCredit = (id) ->
			success = (data, status) ->
				$scope.loadCredits($scope.order.id)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/credits/#{id}").success(success).error(error)

		$scope.loadCredits = (id) ->
			$scope.loading = true

			$http.get("api/api.php/orders/#{id}/credits?_=#{new Date().getTime()}").success (data) ->
				$scope.loading = false				
				$scope.credits = _.map(data, (d) -> new Credit($scope.order, d))
				setTimeout ( ->
					$(".nailthumb-container").nailthumb()
				), 100

		$scope.loadOrder = (id) ->
			$scope.loading = true
			
			success = (data, status) ->
				$scope.id = id
				$scope.loading = false
				window.data = data
				console.log(data)
				$location.path("/orders") if data is "false"

				$scope.order = new Order(data)

				$scope.loadCredits(id)

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)

			$http.get("api/api.php/orders/#{id}?_=#{new Date().getTime()}").success(success).error(error)

		$scope.sendEmail = (id) ->
			id ?= $scope.order.id
			$http.post("api/api.php/orders/#{id}/email").success () ->
				alert("Email sent to #{$scope.order.attributes.email}")

		$scope.noEmail = () ->
			!$scope.order? or _.string.isBlank($scope.order.attributes.email)

		$scope.loadOrder($routeParams.id)
		window.params = $routeParams
		window.scope = $scope

class EditOrderCtrl
	constructor: ($scope, $http, $location, $routeParams, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.order =
			id: $routeParams.id
		
		$scope.loadOrder = (id) ->
			$scope.loading = true
			
			success = (data, status) ->
				$scope.loading = false
				$location.path("/orders") if data is "false"

				console.log(data)
				$scope.order_data = data

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log(data)

			$http.get("api/api.php/orders/#{id}?_=#{new Date().getTime()}").success(success).error(error)

		$scope.saveOrder = (id, data) ->
			success = (data, status) ->
				$location.path("/orders/#{id}")
			error = (data, status) ->
				alert("Something went wrong...")
			$http.post("api/api.php/orders/#{id}", data).success(success).error(error)

		$scope.loadOrder($scope.order.id)

angular.module("pot").controller("OrdersCtrl", ["$scope", "$http", "$location", "Authorize", OrdersCtrl])
angular.module("pot").controller("ViewOrderCtrl", ["$scope", "$http", "$location", "$routeParams", "Authorize", ViewOrderCtrl])
angular.module("pot").controller("EditOrderCtrl", ["$scope", "$http", "$location", "$routeParams", "Authorize", EditOrderCtrl])