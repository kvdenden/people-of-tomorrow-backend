class Design
	constructor: (data) ->
		@data = data # all data

		@id = data.id

		date = moment(data.transaction_date)
		@filename = "http://peopleoftomorrow.com/preview/#{data.filename}"

		@pending = data.last_validation is null
		@accepted = data.last_validation is "1"
		@rejected = data.last_validation is "0"

		@attributes =
			order_id: data.order_id
			transaction_date: if date.isValid() then date.format("ll") else null
			cardholder: data.cardholder
			email: data.email
			amount: "#{data.amount / 100} #{data.currency}"
			status: if data.last_validation is "0" then "<span class='text-danger'>rejected</span>" else if data.last_validation is "1" then "<span class='text-success'>accepted</span>" else "<span class='text-warning'>pending</span>"
			width: data.width
			height: data.height
			type: if data.type then data.type else "<span class='text-danger'>no credit</span>"
			token: data.token
			group_id: if data.group_id is "0" then "" else data.group_id
			filename: "<div class='design-wrapper'><a href='#/validate/#{@id}' target='_blank'><img src='#{@filename}' /></a></div>"

	validate: ($http, value, success, error) ->
		success ?= (data, status) -> ''
		error ?= (data, status) -> ''
		$http.post("api/api.php/designs/#{@id}/validate", {validation_code: value}).success(success).error(error)

	get: (key) ->
		if key of @attributes
			@attributes[key]

class ValidationsCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "validator"

		$scope.designs = []

		$scope.total_designs = 0

		$scope.columns = [
			title: "Thumbnail"
			key: "filename"
			sortable: false
		,
			title: "Type"
			key: "type"
			sortable: true
		,
			title: "Code"
			key: "token"
			sortable: true
		,
			title: "Group ID"
			key: "group_id"
			sortable: true
		,
			title: "Status"
			key: "status"
			sortable: false
		]
		
		$scope.options =
			page: 1
			items_per_page: 25
			sort_by: "designs.id"
			sort_how: "DESC"
			filters:
				from_date: ""
				to_date: ""
				type: ""
				status: "pending"
			search: ""

		$scope.filters =
			type: [
				title: "Show all types"
				key: ""
			,
				title: "Only show normal"
				key: "normal"
			,
				title: "Only show large"
				key: "large"
			,
				title: "Only show group"
				key: "group"
			]
			status: [
				title: "All"
				key: "all"
			,
				title: "Accepted"
				key: "accepted"
			,
				title: "Rejected"
				key: "rejected"
			,
				title: "Pending"
				key: "pending"
			]

		$scope.deleteDesign = (id) ->
			success = (data, status) ->
				alert("Design deleted")
				$scope.loadDesigns($scope.options)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/designs/#{id}").success(success).error(error)

		$scope.loadDesignsWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadDesigns(options)
			), delay

		$scope.loadDesigns = (options) ->

			$scope.loading = true

			options ?= $scope.options

			# from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			# to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			# query += "&from=#{from_date}" if from_date?
			# query += "&to=#{to_date}" if to_date?
			query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&status=#{options.filters.status}" unless _.string.isBlank(options.filters.status)

			success = (data, status) ->
				$scope.loading = false
				$scope.designs = _.map(data, (d) -> new Design(d))
				$scope.$apply() unless $scope.$$phase

				# setTimeout ( ->
				# 	$(".nailthumb-container").nailthumb()
				# ), 100

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)
			$http.get("api/api.php/validate/designs?#{query}&_=#{new Date().getTime()}").success(success).error(error)
			$http.get("api/api.php/validate/designs?#{query}&total=1&_=#{new Date().getTime()}").success (data, status) -> $scope.total_designs = data.total

		$scope.sort = (column) ->
			if _.findWhere($scope.columns, {key: column})
				if $scope.options.sort_by is column
					# reverse sort order
					$scope.options.sort_how = if $scope.options.sort_how is "DESC" then "ASC" else "DESC"
				else
					$scope.options.sort_by = column
					$scope.options.sort_how = "ASC"

				$scope.loadDesigns($scope.options)

		$scope.csvLink = (options) ->
			options ?= $scope.options

			# from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			# to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			# query += "&from=#{from_date}" if from_date?
			# query += "&to=#{to_date}" if to_date?
			query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&status=#{options.filters.status}" unless _.string.isBlank(options.filters.status)
			query += "&csv=1"

			"api/api.php/validate/designs?#{query}"

		$scope.generateCertificate = (design) ->
			$http.post("api/api.php/designs/#{design.id}/certificate").success (data, status) ->
				console.log(data)
				alert("generated certificate")

		$scope.validate = (design, value) ->
			$scope.loading = true
			success = (data, status) ->
				$scope.loadDesigns()
			error = (data, status) ->
				$scope.loading = false
				alert("something went wrong...")

			design.validate $http, value, success, error

		$(".form-control.filter").change () ->
			$scope.options.page = 1
			$scope.loadDesignsWithDelay($scope.options) # hack, why doesn't $watch work???

		$scope.loadDesigns($scope.options)

		window.scope = $scope

class ViewValidationCtrl
	constructor: ($scope, $http, $location, $routeParams, Authorize) ->
		Authorize "admin", "validator"

		$scope.loadDesign = (id) ->
			$scope.loading = true
			
			success = (data, status) ->
				$scope.id = id
				$scope.loading = false
				window.data = data
				console.log(data)
				$location.path("/validate") if data is "false"

				$scope.design = new Design(data)
				$scope.pending = $scope.design.data.last_validation is null

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)

			$http.get("api/api.php/validate/designs/#{id}?_=#{new Date().getTime()}").success(success).error(error)

		$scope.validate = (value) ->
			$scope.loading = true
			$scope.design.validate $http, value, (data, status) ->
				$scope.loadDesign($scope.id)

		$scope.loadDesign($routeParams.id)
		window.scope = $scope

class RandomValidationCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "validator"

		$scope.finished = false

		$scope.loadDesign = () ->
			$scope.loading = true
			
			success = (data, status) ->
				if data is "false"
					$scope.finished = true
				else
					$scope.id = data.id
					$scope.loading = false

					$scope.design = new Design(data)
					$scope.pending = $scope.design.data.last_validation is null

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)

			$http.get("api/api.php/validate/designs/random?_=#{new Date().getTime()}").success(success).error(error)

		$scope.validate = (value) ->
			$scope.loading = true
			loadNext = (data, status) ->
				$scope.loadDesign()
			# $scope.design.validate $http, value, loadNext, loadNext
			$scope.design.validate $http, value
			loadNext()

		$scope.loadDesign()
		window.scope = $scope

angular.module("pot").controller("ValidationsCtrl", ["$scope", "$http", "Authorize", ValidationsCtrl])
angular.module("pot").controller("ViewValidationCtrl", ["$scope", "$http", "$location", "$routeParams", "Authorize", ViewValidationCtrl])
angular.module("pot").controller("RandomValidationCtrl", ["$scope", "$http", "Authorize", RandomValidationCtrl])