class Credit
	constructor: (data) ->
		@data = data # all data

		@id = data.id
		@order_id = data.order_id
		@design_id = data.design_id

		date = moment(data.transaction_date)

		filename = if data.filename then "http://peopleoftomorrow.com/preview/#{data.filename}" else null

		@attributes =
			transaction_date: if date.isValid() then date.format("ll") else null
			# cardholder: data.cardholder
			# email: data.email
			token: data.token
			type: data.type
			group_id: if data.group_id is "0" then "" else data.group_id
			design: if data.filename then "<div class='thumb-wrapper'><a class='nailthumb-container square-thumb' href='#{filename}' target='_blank'><img src='#{filename}' /></a></div>" else ""
			# design: data.design

	get: (key) ->
		if key of @attributes
			@attributes[key]

class CreditsCtrl
	constructor: ($scope, $http, Authorize) ->
		Authorize "admin", "helpdesk"

		$scope.credits = []

		$scope.total_credits = 0

		$scope.columns = [
			title: "Date"
			key: "transaction_date"
			sortable: true
		,
			title: "Code"
			key: "token"
			sortable: true
		,
			title: "Type"
			key: "type"
			sortable: true
		,
			title: "Group ID"
			key: "group_id"
			sortable: true
		,
			title: "Design"
			key: "design"
			sortable: false
		]
		
		$scope.options =
			page: 1
			items_per_page: 25
			sort_by: "transaction_date"
			sort_how: "DESC"
			filters:
				from_date: ""
				to_date: ""
				type: ""
				with_designs: ""
			search: ""

		$scope.filters =
			type: [
				title: "Show all types"
				key: ""
			,
				title: "Only show normal"
				key: "normal"
			,
				title: "Only show large"
				key: "large"
			,
				title: "Only show group"
				key: "group"
			]
			with_designs: [
				title: "With and without designs"
				key: ""
			,
				title: "Only with designs"
				key: "1"
			,
				title: "Only without designs"
				key: "0"
			]

		$scope.deleteCredit = (id) ->
			success = (data, status) ->
				alert("Credit deleted")
				$scope.loadCredits($scope.options)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/credits/#{id}").success(success).error(error)

		$scope.deleteDesign = (id) ->
			success = (data, status) ->
				alert("Design deleted")
				$scope.loadCredits($scope.options)
			error = (data, status) ->
				alert("Something went wrong...")
			$http.delete("api/api.php/designs/#{id}").success(success).error(error)

		$scope.loadCreditsWithDelay = (options, delay) ->
			delay ?= 100
			options ?= $scope.options
			setTimeout ( ->
				$scope.loadCredits(options)
			), delay

		$scope.loadCredits = (options) ->

			$scope.loading = true

			options ?= $scope.options

			from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			query += "&from=#{from_date}" if from_date?
			query += "&to=#{to_date}" if to_date?
			query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&with_designs=#{options.filters.with_designs}" unless _.string.isBlank(options.filters.with_designs)

			success = (data, status) ->
				$scope.loading = false
				$scope.credits = _.map(data, (d) -> new Credit(d))
				$scope.$apply() unless $scope.$$phase

				setTimeout ( ->
					$(".nailthumb-container").nailthumb()
				), 100

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log("error")
				console.log(data)
			$http.get("api/api.php/credits?#{query}&_=#{new Date().getTime()}").success(success).error(error)
			$http.get("api/api.php/credits?#{query}&total=1&_=#{new Date().getTime()}").success (data, status) -> $scope.total_credits = data.total

		$scope.sort = (column) ->
			if _.findWhere($scope.columns, {key: column})
				if $scope.options.sort_by is column
					# reverse sort order
					$scope.options.sort_how = if $scope.options.sort_how is "DESC" then "ASC" else "DESC"
				else
					$scope.options.sort_by = column
					$scope.options.sort_how = "ASC"

				$scope.loadCredits($scope.options)

		$scope.csvLink = (options) ->
			options ?= $scope.options

			from_date = moment(options.filters.from_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.from_date)
			to_date = moment(options.filters.to_date).format("YYYY-MM-DD") unless _.string.isBlank(options.filters.to_date)

			query = "page=#{options.page}&items_per_page=#{options.items_per_page}&sort_by=#{options.sort_by}&sort_how=#{options.sort_how}"
			query += "&from=#{from_date}" if from_date?
			query += "&to=#{to_date}" if to_date?
			query += "&type=#{options.filters.type}" unless _.string.isBlank(options.filters.type)
			query += "&search=#{options.search}" unless _.string.isBlank(options.search)
			query += "&with_designs=#{options.filters.with_designs}" unless _.string.isBlank(options.filters.with_designs)
			query += "&csv=1"

			"api/api.php/credits?#{query}"

		$(".form-control.filter").change () ->
			$scope.options.page = 1
			$scope.loadCreditsWithDelay($scope.options) # hack, why doesn't $watch work???

		$scope.loadCredits($scope.options)

		$("input.date").datepicker()

		window.scope = $scope

angular.module("pot").controller("CreditsCtrl", ["$scope", "$http", "Authorize", CreditsCtrl])