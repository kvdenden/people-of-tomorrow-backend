class User
	constructor: (data, all_roles) ->
		@data = data # all data

		@id = data.id

		user_roles = data.roles.split(",")

		@attributes =
			username: data.username
			email: data.email

		roles = _.map all_roles, (role) =>
			[role.key, _.contains(user_roles, role.key)]

		@roles = _.object roles

	get: (key) ->
		if key of @attributes
			@attributes[key]
			

class UsersCtrl
	constructor: ($scope, $http, $location, Authorize) ->
		Authorize "admin"

		$scope.users = []

		$scope.columns = [
			title: "Username"
			key: "username"
		,
			title: "Email"
			key: "email"
		]

		$scope.roles = [
			title: "Administrator"
			key: "admin"
		,
			title: "Helpdesk"
			key: "helpdesk"
		,
			title: "Validator"
			key: "validator"
		]


		$scope.current_user = 0

		$scope.edit_user =
			id: null
			username: ''
			email: ''
			password: ''
			roles: ''

		$scope.alerts = []

		$scope.loadCurrentUser = () ->
			$http.get("api/api.php/users/current?_=#{new Date().getTime()}").success (data, status) ->
				$scope.current_user = data.id if 'id' of data

		$scope.editUser = (user) ->
			$scope.edit_user.id = user.data.id
			$scope.edit_user.username = user.data.username
			$scope.edit_user.email = user.data.email
			$scope.edit_user.password = ''
			$scope.edit_user.roles = user.data.roles

			$("#user-modal").modal('show')

		$scope.newUser = () ->
			$scope.edit_user.id = null
			$scope.edit_user.username = ''
			$scope.edit_user.email = ''
			$scope.edit_user.password = ''
			$scope.edit_user.roles = ''

			$("#user-modal").modal('show')

		$scope.updateUser = (user) ->
			$scope.loading = true
			data =
				username: user.attributes.username
				email: user.attributes.email

			roles = []
			for role, value of user.roles
				roles.push role if value
			data.roles = roles.join(",")

			console.log("saving user with data")
			console.log(data)

			success = (data, status) ->
				console.log("user saved")
				$scope.loadUsers()

			error = (data, status) ->
				$scope.loading = false
				console.log(data)

			$http.post("api/api.php/users/#{user.id}", data).success(success).error(error)

		$scope.saveUser = () ->
			console.log($scope.edit_user)
			if $scope.edit_user.id
				data =
					username: $scope.edit_user.username
					email: $scope.edit_user.email
					roles: $scope.edit_user.roles

				data.password_hash = md5($scope.edit_user.password) unless _.string.isBlank $scope.edit_user.password

				success = (data, status) ->
					console.log("user updated")
					$("#user-modal").modal('hide')
					$scope.edit_user.id = null
					$scope.edit_user.username = ''
					$scope.edit_user.email = ''
					$scope.edit_user.password = ''
					$scope.edit_user.roles = ''
					$scope.loadUsers()

				error = (data, status) ->
					console.log(data)

				$http.post("api/api.php/users/#{$scope.edit_user.id}", data).success(success).error(error)
			else
				$scope.saveNewUser($scope.edit_user.username, $scope.edit_user.email, $scope.edit_user.password)

		$scope.saveNewUser = (username, email, password) ->
			data =
				username: username
				email: email
				password_hash: md5(password)
				roles: ""

			success = (data, status) ->
				console.log("new user saved")
				$("#user-modal").modal('hide')
				$scope.edit_user.id = null
				$scope.edit_user.username = ''
				$scope.edit_user.email = ''
				$scope.edit_user.password = ''
				$scope.edit_user.roles = ''
				$scope.loadUsers()

			error = (data, status) ->
				if status is 500
					$scope.alert(data, "danger")
				console.log(data)
			$http.post("api/api.php/users", data).success(success).error(error)

		$scope.deleteUser = (user) ->
			$scope.loading = true
			success = (data, status) ->
				console.log("user deleted")
				$scope.loadUsers()

			error = (data, status) ->
				$scope.loading = false
				console.log(data)
			$http.delete("api/api.php/users/#{user.id}").success(success).error(error)

		$scope.loadUsers = () ->
			$scope.loading = true

			success = (data, status) ->
				$scope.loading = false
				$scope.users = _.map(data, (d) -> new User(d, $scope.roles))

				$scope.$apply() unless $scope.$$phase

				# console.log(data)
			error = (data, status) ->
				$scope.loading = false
				console.log(data)

			$http.get("api/api.php/users?_=#{new Date().getTime()}").success(success).error(error)

		$scope.alert = (message, type, delay) ->
			al = 
				message: message
				type: type or 'info'
			$scope.alerts.push al
			if delay > 0
				setTimeout ( -> 
					idx = $scope.alerts.indexOf(al)
					$scope.alerts.splice(idx, 1)
					$scope.$apply() unless $scope.$$phase
				), delay
			$scope.$apply() unless $scope.$$phase

		$scope.closeAlert = (alert) ->
			idx = $scope.alerts.indexOf(alert)
			$scope.alerts.splice(idx, 1)
			$scope.$apply() unless $scope.$$phase

		$scope.loadCurrentUser()
		$scope.loadUsers()
		window.scope = $scope


angular.module("pot").controller("UsersCtrl", ["$scope", "$http", "$location", "Authorize", UsersCtrl])