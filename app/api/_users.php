<?php

	/** 
	[method] : POST
	[url] : /login
	*/
	$app->post('/login', function () use (&$db, $app) {
		$_SESSION['user'] = null;
		$params = json_decode(file_get_contents('php://input'), true);
		$app->log->debug("[POST]/login ". json_encode($params));

		$username = $params['username'];
		$password_hash = $params['password_hash'];

		$q = "select id, username, roles from users where username = :username and password_md5 = :password_hash";
		$query = $db->prepare($q);
		$query->bindValue(':username', $username, PDO::PARAM_STR);
		$query->bindValue(':password_hash', $password_hash, PDO::PARAM_STR);
		$query->execute();
		$user = $query->fetch(PDO::FETCH_ASSOC);

		if($user){
			$_SESSION['user'] = $user;
			$app->log->debug("");
			echo json_encode($user);
		}else{
			$app->log->debug("*ERROR* user/pwd not found");			
			$app->response->setStatus(500);
			$app->log->debug("");
			echo json_encode(array());
		}

	});

	/** 
	[method] : GET
	[url] : /users/current
	*/
	$app->get('/users/current', function () use (&$db, $app) {

		if(isset($_SESSION['user'])){
			$app->log->debug("[GET]/users/current ". $_SESSION['user']['username']);
			echo json_encode($_SESSION['user']);
			$app->log->debug("");
		}else{
			$app->log->debug("[GET]/user/current anonymous");
			$app->log->debug("");
			echo json_encode(array());
		}
	});

	/** 
	[method] : GET
	[url] : /users/current
	*/
	$app->post('/users/current', function () use (&$db, $app) {
		$params = json_decode(file_get_contents('php://input'), true);
		$app->log->debug("[POST]/users/current ". json_encode($params));

		if(isset($_SESSION['user'])){
			$current_user = $_SESSION['user'];
			$id = $current_user['id'];
			$username = $current_user['username'];

			$app->log->debug("current_user". json_encode($current_user));


			$new_username = $params['username'];
			$password_hash = isset($params['password_hash']) ? $params['password_hash'] : null;
			$email = $params['email'];

			$q = "select * from users where id != :id and (username = :username or email = :email)";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int)$id, PDO::PARAM_INT );
			$query->bindValue(':username', $new_username, PDO::PARAM_STR );
			$query->bindValue(':email', $email, PDO::PARAM_STR );
			$query->execute();
			$exists = $query->fetch(PDO::FETCH_ASSOC);
			
			if($exists){
				$app->log->debug("can not change username/email. Already exists");
				$app->log->debug("");
				$app->response->setStatus(500);
				return;
			}

			$app->log->debug("update users set username = ".$new_username.", email = ". $email ." where username = ".$username);
			$q = "update users set username = :new_username, email = :email where username = :username";

			$query = $db->prepare($q);
			$query->bindValue(':new_username', $new_username, PDO::PARAM_STR );
			$query->bindValue(':username', $username, PDO::PARAM_STR );
			$query->bindValue(':email', $email, PDO::PARAM_STR );
			$query->execute();


			if($password_hash != null){
				$app->log->debug("update users set password_md5 = md5 where username = ".$username);
				$q = "update users set password_md5 = :password_md5 where username = :username";
				$query = $db->prepare($q);
				$query->bindValue(':username', $new_username, PDO::PARAM_STR );
				$query->bindValue(':password_md5', $password_hash, PDO::PARAM_STR );
				$query->execute();
			}

			$q = "select id, username, roles from users where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();
			$user = $query->fetch(PDO::FETCH_ASSOC);

			if($user){
				$_SESSION['user'] = $user;
			}

			$app->log->debug("");

		}else{
			$app->log->debug("*ERROR* no session found");			
			$app->response->setStatus(500);
		}
	});


	/** 
	[method] : GET
	[url] : /logout
	*/
	$app->get('/logout', function () use (&$db, $app) {
		
		if(isset($_SESSION['user'])){
			$app->log->debug("[GET]/logout". $_SESSION['user']['username']);
		}else{
			$app->log->debug("[GET]/logout anonymous");
		}
		$app->log->debug("");

		$_SESSION = array();
		session_destroy();
	});

	/** 
	[method] : POST
	[url] : /users
	*/
	$app->post('/users', function () use (&$db, $app) {
		$params = json_decode(file_get_contents('php://input'), true);
		$app->log->debug("[POST]/users ". json_encode($params));

		if(!allowed(array("admin"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$username = $params['username'];
		$password_hash = $params['password_hash'];
		$email = $params['email'];
		$roles = $params['roles'];

		$q = "select * from users where username = :username or email = :email";
		$query = $db->prepare($q);
		$query->bindValue(':username', $username, PDO::PARAM_STR );
		$query->bindValue(':email', $email, PDO::PARAM_STR );		
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);
		
		if($exists){
			$app->log->debug("*ERROR* user with username/email ". $username."/".$email. " already exists");
			$app->log->debug("");
			$app->response->setStatus(500);
			echo "user with same username or email already exists";
			return;
		}else{
			$app->log->debug("insert into users (username, email, roles, password_md5) values (".$username.", ".$email.", ".$roles.", md5)");

			$q = "insert into users (username, email, roles, password_md5) values (:username, :email, :roles, :password_md5)";

			$query = $db->prepare($q);
			$query->bindValue(':username', $username, PDO::PARAM_STR );
			$query->bindValue(':email', $email, PDO::PARAM_STR );
			$query->bindValue(':roles', $roles, PDO::PARAM_STR );
			$query->bindValue(':password_md5', $password_hash, PDO::PARAM_STR );
			$query->execute();			
		}
		$app->log->debug("");

	});

	/** 
	[method] : GET
	[url] : /users
	*/
	$app->get('/users', function () use (&$db, $app) {
		$app->log->debug("[GET]/users");

		if(!allowed(array("admin"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$app->log->debug("select id, username, email, roles from users order by id");
		$q = "select id, username, email, roles from users order by id";

		$query = $db->prepare($q);
		$query->execute();
		$users = $query->fetchAll(PDO::FETCH_ASSOC);
		
		echo json_encode($users);

		$app->log->debug("");

	});

	/** 
	[method] : DELETE
	[url] : /users/:id
	*/
	$app->delete('/users/:id', function ($id) use (&$db, $app) {
		$app->log->debug("[DELETE]/users/".$id);

		if(!allowed(array("admin"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$app->log->debug("delete from users where id = ".$id);
		$q = "delete from users where id = :id";

		$query = $db->prepare($q);
		$query->bindValue(':id', $id, PDO::PARAM_INT );
		$query->execute();

		$app->log->debug("");

	});

	/** 
	[method] : POST
	[url] : /users/:id
	*/
	$app->post('/users/:id', function ($id) use (&$db, $app) {
		$app->log->debug("[POST]/users/".$id);

		if(!allowed(array("admin"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$params = json_decode(file_get_contents('php://input'), true);
		
		$username = $params['username'];
		$password_hash = isset($params['password_hash']) ? $params['password_hash'] : null;
		$email = $params['email'];
		$roles = $params['roles'];


		$q = "select 1 from users where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int)$id, PDO::PARAM_INT );
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);

		if(!$exists) {
			$app->log->debug("User does not exist");
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}

		$q = "select * from users where id != :id and (username = :username or email = :email)";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int)$id, PDO::PARAM_INT );
		$query->bindValue(':username', $username, PDO::PARAM_STR );
		$query->bindValue(':email', $email, PDO::PARAM_STR );
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);
		
		if($exists){
			$app->log->debug("can not change username/email. Already exists");
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}
		

		$app->log->debug("update users set username = ".$username.", email = ". $email .", roles = ". $roles ." where id = ".$id);
		$q = "update users set username = :username, email = :email, roles = :roles where id = :id";

		$query = $db->prepare($q);
		$query->bindValue(':username', $username, PDO::PARAM_STR );
		$query->bindValue(':id', (int)$id, PDO::PARAM_INT );
		$query->bindValue(':email', $email, PDO::PARAM_STR );
		$query->bindValue(':roles', $roles, PDO::PARAM_STR );
		$query->execute();


		if($password_hash != null){
			$app->log->debug("update users set password_md5 = md5 where id = ".$id);
			$q = "update users set password_md5 = :password_md5 where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int)$id, PDO::PARAM_INT );
			$query->bindValue(':password_md5', $password_hash, PDO::PARAM_STR );
			$query->execute();
		}

		$app->log->debug("");

	});

?>