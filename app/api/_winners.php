<?php

	/** 
	[method] : GET 
	[url] : /winners  
	*/
	$app->get('/winners', function () use (&$db, $app) {


		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$flow = isset($_GET['flow']) && !empty($_GET['flow']) ? $_GET['flow'] : 'all';

		$q = "select orders.transaction_date, orders.flow, orders.cardholder, orders.email, designs.filename
					from designs join credits on designs.credit_id = credits.id join orders on credits.order_id = orders.id
					where :flow = 'all' or orders.flow = :flow";
			
		$query = $db->prepare($q);
		$query->bindValue(':flow', $flow, PDO::PARAM_STR );


		$query->execute();

		echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		// $data = $query->fetchAll(PDO::FETCH_ASSOC);
		// if(sizeof($data) > 0){
		// 	$app->response->headers->set('Content-Type', 'application/octet-stream');
		// 	$app->response->headers->set("Content-Disposition", "attachment; filename=flow-{$flow}.csv");
		// 	$app->response->headers->set("Pragma", "no-cache");
		// 	$app->response->headers->set("Expires", "0");				
		// 	//titles
		// 	echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
		// 	//data
		// 	foreach ($data as $line){
		// 		echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
		// 	}
		// }
	  
	});
?>
