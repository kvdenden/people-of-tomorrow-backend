<?php

require_once '../vendor/autoload.php';

require_once dirname(__FILE__) . '/_util.php';

require_once dirname(__FILE__) . '/db.php';

require_once dirname(__FILE__) . '/critsend.php';

require_once dirname(__FILE__) . '/pdf/fpdf.php';
require_once dirname(__FILE__) . '/pdf/fpdf_tpl.php';
require_once dirname(__FILE__) . '/pdf/fpdi.php';

date_default_timezone_set("Europe/Brussels");

session_start();

$logWriter = new \Slim\LogWriter(fopen(dirname(__FILE__)."/slim.log", 'a'));
$app = new \Slim\Slim(array('log.writer' => $logWriter));

function allowed($roles) {
	global $app;
	if(isset($_SESSION) && isset($_SESSION['user']) ){
		$user_roles_str = $_SESSION['user']['roles'];
		$user_roles = explode(",", $user_roles_str);
		foreach($roles as $role){
			if(in_array($role, $user_roles)){
				$app->log->debug("(method allowed for ".$role.")");
				return true;
			}
		}
	}
	$app->log->debug("(method disallowed)");
	return false;
}

require_once dirname(__FILE__) . '/_orders.php';
require_once dirname(__FILE__) . '/_credits.php';
require_once dirname(__FILE__) . '/_designs.php';
require_once dirname(__FILE__) . '/_neighbours.php';
require_once dirname(__FILE__) . '/_exports.php';
require_once dirname(__FILE__) . '/_users.php';
require_once dirname(__FILE__) . '/_winners.php';

$app->run();

?>