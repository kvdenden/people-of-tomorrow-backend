<?php

	/** 
	[method] : GET 
	[url] : /credits  
	*/
	$app->get('/credits', function () use (&$db, $app) {


		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "orders.transaction_date";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		$type = isset($_GET['type'])? $_GET['type'] : "null";
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		$first = ($page-1) * $items_per_page;
		$from = isset($_GET['from']) ? $_GET['from'] : '0000-00-00';
		$to =  isset($_GET['to']) ? $_GET['to'] : date("Y-m-d");
		$total = isset($_GET['total']) ? $_GET['total'] : 0;
		$with_designs = isset($_GET['with_designs']) ? $_GET['with_designs'] : 'null';
		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$q = "";
		if($csv == 1){
			$q = "select credits.id, credits.order_id, designs.id as design_id, orders.transaction_date, credits.token, credits.type, credits.group_id, designs.filename
				  from orders join credits on orders.id = credits.order_id left join designs on credits.id = designs.credit_id
				  where 
					(lower(token) like :search or lower(group_id) like :search )
					and ( :type = 'null' or type = :type ) 
					and orders.transaction_date between :from and :to
					and (
						(:with_designs = '1' and designs.id is not null)
						or
						(:with_designs = '0' and designs.id is null)
						or
						(:with_designs = 'null')
						)
				  order by {$order_by} {$order_how}";
		}
		elseif($total == 1){
			$q = "select count(distinct(credits.id)) as total
				  from orders join credits on orders.id = credits.order_id left join designs on credits.id = designs.credit_id
				  where 
					(lower(token) like :search or lower(group_id) like :search )
					and ( :type = 'null' or type = :type ) 
					and orders.transaction_date between :from and :to
					and (
						(:with_designs = '1' and designs.id is not null)
						or
						(:with_designs = '0' and designs.id is null)
						or
						(:with_designs = 'null')
						)
				  order by {$order_by} {$order_how}";
		}else{
			$q = "select credits.id, credits.order_id, designs.id as design_id, orders.transaction_date, credits.token, credits.type, credits.group_id, designs.filename
				  from orders join credits on orders.id = credits.order_id left join designs on credits.id = designs.credit_id
				  where 
					(lower(token) like :search or lower(group_id) like :search )
					and ( :type = 'null' or type = :type ) 
					and orders.transaction_date between :from and :to
					and (
						(:with_designs = '1' and designs.id is not null)
						or
						(:with_designs = '0' and designs.id is null)
						or
						(:with_designs = 'null')
						)
				  order by {$order_by} {$order_how}
				  limit :first , :items_per_page";
		}
			
		$query = $db->prepare($q);

		if($total != 1 && $csv != 1) {
			$query->bindValue(':first', (int) $first, PDO::PARAM_INT);
			$query->bindValue(':items_per_page', (int) $items_per_page, PDO::PARAM_INT);
		}
		$query->bindValue(':search', $search, PDO::PARAM_STR );
		$query->bindValue(':type', $type, PDO::PARAM_STR );
		$query->bindValue(':from', $from, PDO::PARAM_STR );
		$query->bindValue(':to', $to, PDO::PARAM_STR );
		$query->bindValue(':with_designs', $with_designs, PDO::PARAM_STR );


		$query->execute();

		if($csv == 1){
			$data = $query->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=credits.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}
		elseif($total == 1){
			echo json_encode($query->fetch(PDO::FETCH_ASSOC));
		}else{
			echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		}
	  
	});

	/** 
	[method] : DELETE 
	[url] : /credits/:id
	*/
	$app->delete('/credits/:id', function ($id) use (&$db, $app) {
		
		$app->log->debug("[DELETE]/credits/".$id);

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

    	

		$q = "select 1 from credits where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);
		
				
		if($exists){
			$app->log->debug("credit with id " . $id . " exists");
			
			// delete attached designs
			$app->log->debug("delete from designs where credit_id = ".$id);				
				
			$q = "delete from designs where credit_id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();


			// delete attached credits
			$app->log->debug("delete from credits where id = ".$id);

			$q = "delete from credits where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();

			
		}else{
			$app->log->debug("*ERROR* no credit with id ".$id);			
			$app->response->setStatus(500);
		}
		$app->log->debug("");

	});

	/** 
	[method] : POST
	[url] : /orders/:id/credit
	*/
	$app->post('/orders/:id/credit', function ($id) use (&$db, $app) {
    	$app->log->debug("[POST]/orders/".$id."/credit");

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

    	$params = json_decode(file_get_contents('php://input'), true);
    	if(!isset($params['type'])){
    		$app->log->debug("*ERROR* no type in POST");			
			$app->response->setStatus(500);
			return;
    	}
    	

    	// test if order exists
    	$q = "select 1 from orders where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);

		if(!$exists){
			$app->log->debug("*ERROR* no order with id ".$id);			
			$app->response->setStatus(500);
			return;
		}

    	$type = $params['type'];
    	$app->log->debug("POST[type]=".$type);


    	$group_id = "";
    	if(isset($params['group_id'])){
    		$group_id = $params['group_id'];
    	}
		
		$app->log->debug("POST[group_id]=".$group_id);

		$amount = 1;
		if(isset($params['amount'])) {
			$amount = (int)$params['amount'];
		}

		$app->log->debug("generating ".$amount." tokens");
		while($amount > 0) {

			$tries = 100;
			while($tries > 0){
				$app->log->debug("generating token... attempt #".(101-$tries));
				//generate token
				$random = rand ( 1, 999 );
        $token = mb_substr ( (string)sha1 ( $id . "-$random" ), 0, 10 );
        $app->log->debug("generated_token=".$token);

        // test if token exists
        $q = "select 1 from credits where token = :token limit 1";
				$query = $db->prepare($q);
				$query->bindValue(':token', $token, PDO::PARAM_STR);
				$query->execute();
				$exists = $query->fetch(PDO::FETCH_ASSOC);

				if(!$exists){
					$app->log->debug("insert into credits (order_id, token, type, group_id) values (".$id.", ".$token.", ".$type.", ".$group_id.")");
					$q = "insert into credits (order_id, token, type, group_id) values (:order_id, :token, :type, :group_id)";
					$query = $db->prepare($q);
					$query->bindValue(':order_id', $id, PDO::PARAM_INT);
					$query->bindValue(':token', $token, PDO::PARAM_STR);
					$query->bindValue(':type', $type, PDO::PARAM_STR);
					$query->bindValue(':group_id', $group_id, PDO::PARAM_STR);
					$query->execute();
					break;
				}else {
					$tries -= 1;
				}
			}
			$amount -= 1;

		}
        

	});
?>
