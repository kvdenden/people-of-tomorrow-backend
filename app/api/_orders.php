<?php
	/** 
	[method] : GET
	[url] : /orders
	*/
	$app->get('/orders', function () use (&$db, $app) {
		
		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "cardholder";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		$paid = isset($_GET['paid'])? $_GET['paid'] : "null";
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		$first = ($page-1) * $items_per_page;
		$from = isset($_GET['from']) ? $_GET['from'] : '0000-00-00';
		$to =  isset($_GET['to']) ? $_GET['to'] : date("Y-m-d");
		$total = isset($_GET['total']) ? $_GET['total'] : 0;
		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$q = "";

		if($csv == 1){
			$q = "select orders.id, orders.order_id, transaction_date, cardholder, email, amount, currency, paid , count(credits.id) as number_of_credits
				from orders left join credits on orders.id = credits.order_id
				where 
					(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
					and ( :paid = 'null' or paid = :paid ) 
					and transaction_date between :from and :to
				group by orders.id, transaction_date, cardholder, email, amount, currency, paid
				order by {$order_by} {$order_how}";
		}
		elseif($total == 1){
			$q = "select count(distinct(orders.id)) as total
				from orders 
				where 
					(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
					and ( :paid = 'null' or paid = :paid ) 
					and transaction_date between :from and :to";
				
		}else {
			$q = 	"select orders.id, orders.order_id, transaction_date, cardholder, email, amount, currency, paid , count(credits.id) as number_of_credits
				from orders left join credits on orders.id = credits.order_id
				where 
					(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
					and ( :paid = 'null' or paid = :paid ) 
					and transaction_date between :from and :to
				group by orders.id, transaction_date, cardholder, email, amount, currency, paid
				order by {$order_by} {$order_how}
				limit :first , :items_per_page";
		}

			
		$query = $db->prepare($q);

		if($total != 1 && $csv != 1) {
			$query->bindValue(':first', (int) $first, PDO::PARAM_INT);
			$query->bindValue(':items_per_page', (int) $items_per_page, PDO::PARAM_INT);
		}

		$query->bindValue(':search', $search, PDO::PARAM_STR );
		$query->bindValue(':paid', $paid, PDO::PARAM_STR );
		$query->bindValue(':from', $from, PDO::PARAM_STR );
		$query->bindValue(':to', $to, PDO::PARAM_STR );
		
		$query->execute();

		if($csv == 1){
			$data = $query->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=orders.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}
		elseif($total == 1){
			echo json_encode($query->fetch(PDO::FETCH_ASSOC));
		}else{
			echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		}
	  
	});

$app->get('/orders/test', function () use (&$db, $app) {
		
		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "cardholder";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		$paid = isset($_GET['paid'])? $_GET['paid'] : "null";
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		$first = ($page-1) * $items_per_page;
		$from = isset($_GET['from']) ? $_GET['from'] : '0000-00-00';
		$to =  isset($_GET['to']) ? $_GET['to'] : date("Y-m-d");
		$total = isset($_GET['total']) ? $_GET['total'] : 0;
		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$q = "";

		if($csv == 1){
			$q = "select orders.id, orders.order_id, transaction_date, cardholder, email, amount, currency, paid , count(credits.id) as number_of_credits
				from orders left join credits on orders.id = credits.order_id
				where 
					(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
					and ( :paid = 'null' or paid = :paid ) 
					and transaction_date between :from and :to
				group by orders.id, transaction_date, cardholder, email, amount, currency, paid
				order by {$order_by} {$order_how}";
		}
		elseif($total == 1){
			$q = "select count(distinct(orders.id)) as total
				from orders 
				where 
					(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
					and ( :paid = 'null' or paid = :paid ) 
					and transaction_date between :from and :to";
				
		}else {
			$q = 	"select orders.id, orders.order_id, transaction_date, cardholder, email, amount, currency, paid , count(credits.id) as number_of_credits
				from orders left join credits on orders.id = credits.order_id
				where 
					(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
					and ( :paid = 'null' or paid = :paid ) 
					and transaction_date between :from and :to
				group by orders.id, transaction_date, cardholder, email, amount, currency, paid
				order by {$order_by} {$order_how}
				limit :first , :items_per_page";
		}

			
		$query = $db->prepare($q);

		if($total != 1 && $csv != 1) {
			$query->bindValue(':first', (int) $first, PDO::PARAM_INT);
			$query->bindValue(':items_per_page', (int) $items_per_page, PDO::PARAM_INT);
		}

		$query->bindValue(':search', $search, PDO::PARAM_STR );
		$query->bindValue(':paid', $paid, PDO::PARAM_STR );
		$query->bindValue(':from', $from, PDO::PARAM_STR );
		$query->bindValue(':to', $to, PDO::PARAM_STR );
		
		$query->execute();

		if($csv == 1){
			$data = $query->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=orders.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}
		elseif($total == 1){
			echo json_encode($query->fetch(PDO::FETCH_ASSOC));
		}else{
			echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		}
	  
	});

	/** 
	[method] : GET
	[url] : /orders/summary
	*/
	$app->get('/orders/summary', function () use (&$db, $app) {
		
		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = "transaction_date";
		$order_how = "desc";
		$paid = isset($_GET['paid'])? $_GET['paid'] : "null";
		// $page = isset($_GET['page']) ? $_GET['page'] : 1;
		// $items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		// $first = ($page-1) * $items_per_page;
		$from = isset($_GET['from']) ? $_GET['from'] : '0000-00-00';
		$to =  isset($_GET['to']) ? $_GET['to'] : date("Y-m-d");
		// $total = isset($_GET['total']) ? $_GET['total'] : 0;
		// $csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$q = "select transaction_date, orders.order_id, cardholder, email, amount,
			(select count(*) from credits where credits.order_id = orders.id and credits.type = 'normal') as normal_credits,
			(select count(*) from credits where credits.order_id = orders.id and credits.type = 'large') as large_credits,
			(select count(*) from credits where credits.order_id = orders.id and credits.type = 'group') as group_credits
			from orders
			where 
				(lower(orders.order_id) like :search or lower(email) like :search or lower(cardholder) like :search )
				and ( :paid = 'null' or paid = :paid ) 
				and transaction_date between :from and :to
			group by orders.id, transaction_date, cardholder, email, amount, currency, paid
			order by {$order_by} {$order_how}";

			
		$query = $db->prepare($q);

		$query->bindValue(':search', $search, PDO::PARAM_STR );
		$query->bindValue(':paid', $paid, PDO::PARAM_STR );
		$query->bindValue(':from', $from, PDO::PARAM_STR );
		$query->bindValue(':to', $to, PDO::PARAM_STR );
		
		$query->execute();

		$data = $query->fetchAll(PDO::FETCH_ASSOC);
		if(sizeof($data) > 0){
			$app->response->headers->set('Content-Type', 'application/octet-stream');
			$app->response->headers->set("Content-Disposition", "attachment; filename=summary.csv");
			$app->response->headers->set("Pragma", "no-cache");
			$app->response->headers->set("Expires", "0");				
			//titles
			echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
			//data
			foreach ($data as $line){
				echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
			}
		}
		
	  
	});

	/** 
	[method] : POST
	[url] : /orders
	*/
	$app->post('/orders', function () use (&$db, $app) {
		$app->log->debug("[POST]/orders");

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$ip = '';
		if (! isset ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
			$ip = $_SERVER ['REMOTE_ADDR'];
		} else {
			$ip = $_SERVER ['HTTP_X_FORWARDED_FOR'];
		}

		$params = json_decode(file_get_contents('php://input'), true);

		$app->log->debug("creating new order");
		$q = "insert into orders (order_id, cardholder, email, transaction_date) values (:order_id, :cardholder, :email, :transaction_date)";

		$random = rand ( 1, 99999 );
		$ip = str_pad ( substr ( preg_replace ( "/[^0-9]/", "", $ip ) * $random, 3, 6 ), 6, '0', STR_PAD_LEFT );
		$stamp = strtotime ( "now" );
		$order_id = "POT-$stamp-$ip";

		$query = $db->prepare($q);
		$query->bindValue(':order_id', $order_id, PDO::PARAM_STR);
		$query->bindValue(':cardholder', $params['cardholder'], PDO::PARAM_STR);
		$query->bindValue(':email', $params['email'], PDO::PARAM_STR);
		$query->bindValue(':transaction_date', date("Y-m-d"), PDO::PARAM_STR);

		$result = $query->execute();
		echo json_encode(array('id' => $db->lastInsertId()));

	});

	/** 
	[method] : GET
	[url] : /orders/:id
	*/
	$app->get('/orders/:id', function($id) use (&$db, $app) {

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select *
			from orders
			where orders.id = :id";

		$query = $db->prepare($q);

		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);

		$query->execute();

		echo json_encode($query->fetch(PDO::FETCH_ASSOC));

	});

	/**
	[method] : POST
	[url] : /orders/:id/email
	*/

	$app->post('/orders/:id/email', function($id) use (&$db, $app) {
		$app->log->debug("[POST]/orders/".$id."/email");
		// order cardholder, order email
		$query = $db->prepare("select cardholder, email from orders where orders.id = :id");
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();

		$order = $query->fetch(PDO::FETCH_ASSOC);

		$email = isset($order['email']) ? trim($order['email']) : null;
		
		if(!$email || empty($email)) {
			$app->log->debug("*ERROR* no email found for order ".$id);		
			$app->log->debug("");
			$app->response->setStatus(500);
			echo "No email found for order ".$id;
			return;
		}
		
		// data for template
		$data = array(
			'cardholder' => trim($order['cardholder']),
			'credits' => array(
				'normal' => [],
				'large' => [],
				'group' => []
			)
		);

		// TODO: list of credits, grouped by type (credit, type, group_id, has_design)
		$query = $db->prepare("select token, type, group_id from credits where credits.order_id = :id");
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();

		$credits = $query->fetchAll(PDO::FETCH_ASSOC);
		foreach ($credits as $credit) {
			$type = $credit['type'];
			array_push($data['credits'][$type], array('code' => $credit['token'], 'group_id' => $credit['group_id']));
		}

		$template = 'mails/order.php';

		ob_start();
		include $template;
		$email_body = ob_get_contents();
		ob_end_clean();

		// $app->log->debug($email_body);

		$content = array('subject'=> 'Your credits | People Of Tomorrow', 'html' => $email_body, 'text' => $email_body);

		$param = array(
			'tag' => array('credits'), 
			'mailfrom' => 'contact@peopleoftomorrow.com',
			'mailfrom_friendly' => 'People Of Tomorrow', 
			'replyto' => 'contact@peopleoftomorrow.com', 
			'replyto_filtered' => 'true'
		);

		$emails = array(array('email' => $email));

		$critsend = new MxmConnect();
		$success = $critsend->sendCampaign($content, $param, $emails);
		echo json_encode(array('success' => $success));

	});


	/** 
	[method] : GET
	[url] : /orders/:id/credits
	*/
	$app->get('/orders/:id/credits', function($id) use (&$db, $app) {

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;
		
		$q = "select credits.id, token, type, group_id, filename, designs.id as design_id
				from 
				orders join credits on orders.id = credits.order_id
				left join designs on credits.id = designs.credit_id
				where orders.id = :id";



		$query = $db->prepare($q);

		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);

		$query->execute();

		$data = $query->fetchAll(PDO::FETCH_ASSOC);
		if($csv == 1){
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=credits_of_order.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}else{
			echo json_encode($data);
		}
		

	});

	/** 
	[method] : DELETE
	[url] : /orders/:id
	*/
	$app->delete('/orders/:id', function ($id) use (&$db, $app) {
    	$app->log->debug("[DELETE]/orders/".$id);

		$q = "select 1 from orders where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);
		
				
		if($exists){
			$app->log->debug("order with id " . $id . " exists");
			// delete attached designs
			$app->log->debug("");
			$q = "select id from credits where credits.order_id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();
			$credit_ids = $query->fetchAll(PDO::FETCH_COLUMN, 0);

			foreach($credit_ids as $credit_id){
				
				$app->log->debug("delete from designs where credit_id = ".$credit_id);				
				
				$q = "delete from designs where credit_id = :credit_id";
				$query = $db->prepare($q);
				$query->bindValue(':credit_id', (int) $credit_id, PDO::PARAM_INT);
				$query->execute();


			}

			// delete attached credits
			$app->log->debug("delete from credits where credits.order_id = ".$id);

			$q = "delete from credits where credits.order_id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();

			// delete order
			$app->log->debug("delete from orders where id = ".$id);

			$q = "delete from orders where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();			
		}else{
			$app->log->debug("*ERROR* no order with id ".$id);			
			$app->response->setStatus(500);
		}
		$app->log->debug("");

	});


	/** 
	[method] : POST
	[url] : /orders/:id
	*/
	$app->post('/orders/:id', function ($id) use (&$db, $app) {
    $app->log->debug("[POST]/orders/".$id);

    if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

    $params = json_decode(file_get_contents('php://input'), true);

    // test if order exists
    $q = "select 1 from orders where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$order = $query->fetch(PDO::FETCH_ASSOC);

		if($order){
			
			$app->log->debug("updating order");
			$q = "update orders 
					set 
					status = :status, 
					paid = :paid, 
					order_id = :order_id, 
					cardholder = :cardholder, 
					email = :email, 
					pay_id = :pay_id,
					transaction_date = :transaction_date,
					amount = :amount,
					currency = :currency,
					language = :language,
					flow = :flow
				where id = :id";

			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->bindValue(':status', $params['status'], PDO::PARAM_STR);
			$query->bindValue(':paid', (int) $params['paid'], PDO::PARAM_STR);
			$query->bindValue(':order_id', $params['order_id'], PDO::PARAM_STR);
			$query->bindValue(':cardholder', $params['cardholder'], PDO::PARAM_STR);
			$query->bindValue(':email', $params['email'], PDO::PARAM_STR);
			$query->bindValue(':pay_id', $params['pay_id'], PDO::PARAM_STR);
			$query->bindValue(':transaction_date', $params['transaction_date'], PDO::PARAM_STR);
			$query->bindValue(':amount', (int) $params['amount'], PDO::PARAM_INT);
			$query->bindValue(':currency', $params['currency'], PDO::PARAM_STR);
			$query->bindValue(':language', $params['language'], PDO::PARAM_STR);
			$query->bindValue(':flow', $params['flow'], PDO::PARAM_STR);


			$query->execute();

		}else{
			$app->log->debug("*ERROR* no order with id ".$id);		
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}
        


	});
?>
