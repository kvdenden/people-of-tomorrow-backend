<?php

	/** 
	[method] : GET
	[url] : /exports
	*/
	$app->get('/exports', function () use (&$db, $app) {

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}
		
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "creation_date";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		
		$q = "select id, filename, creation_date from exports order by {$order_by} {$order_how}";
			
		$query = $db->prepare($q);

		$query->execute();

		echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		
	});



		/** 
	[method] : DELETE 
	[url] : /exports/:id
	*/
	$app->delete('/exports/:id', function ($id) use (&$db, $app) {
		
		$app->log->debug("[DELETE]/exports/".$id);

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

    	

		$q = "select 1 from exports where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);
		
				
		if($exists){
			$app->log->debug("export with id " . $id . " exists");
			
			$app->log->debug("delete from exports where id = ".$id);

			$q = "delete from exports where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();

			
		}else{
			$app->log->debug("*ERROR* no exports with id ".$id);			
			$app->response->setStatus(500);
		}
		$app->log->debug("");

	});



/** 
	[method] : POST
	[url] : /exports
	*/
	$app->post('/exports', function () use (&$db, $app) {
		$app->log->debug("[POST]/exports");


		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$params = json_decode(file_get_contents('php://input'), true);

		if(!isset($params['limit']) || !is_numeric($params['limit'])){
			$app->log->debug("*ERROR* no valid limit in post");
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}

		$limit = (int) $params['limit'];
		if($limit < 0){
			$limit = 0;
		}

		$q = "select 
					designs.id, 
					designs.filename, 
					(select value 
						from validations 
						where 
							design_id = designs.id 
							and validations.timestamp = (select max(validations.timestamp) from validations where design_id = designs.id)) last_validation 
				from 
					designs
				where
					(designs.id not in (select distinct design_id from exported_designs))
				having last_validation = 1
				order by rand() limit {$limit}";

		$query = $db->prepare($q);
		
		$query->execute();
		$designs = $query->fetchAll(PDO::FETCH_ASSOC);

		if(sizeof($designs) < 1) {
				$app->log->debug("*WARNING* no exports available");
				$app->log->debug("");
				$app->response->setStatus(204);
				return;
		}

		$exports_dir = "../exports/";
		$designs_dir = "../designs/";
		$zip_filename = md5(rand()) . '.zip';
		while(file_exists($exports_dir . $zip_filename)) {
			$zip_filename = md5(rand()) . '.zip';
		}

		// save export to db and get its id
		$q = "insert into exports (filename, creation_date) values (:filename, :creation_date)";
		$query = $db->prepare($q);
		$query->bindValue(':filename', $zip_filename, PDO::PARAM_STR);
		$query->bindValue(':creation_date', date("Y-m-d-h-m-s"), PDO::PARAM_STR);
		$query->execute();
	
		$export_id = $db->lastInsertId();

		$zip = new ZipArchive();
		$zip->open($exports_dir . $zip_filename, ZipArchive::CREATE);

		foreach ($designs as $design) {
			$filename = $design['filename'];
			$app->log->debug("add " . $designs_dir . $filename . " to zip");
			$zip->addFile($designs_dir . $filename, $filename);

			// add item with this design_id and export_id to exported_designs
			$q = "insert into exported_designs (design_id, export_id) values (:design_id, :export_id)";
			$query = $db->prepare($q);
			$query->bindValue(':design_id', $design['id'], PDO::PARAM_INT);
			$query->bindValue(':export_id', $export_id, PDO::PARAM_INT);
			$query->execute();
		}

		$zip->close();

		echo $zip_filename;
	});




?>
