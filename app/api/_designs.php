<?php

	/** 
	[method] : GET
	[url] : /designs
	*/
	$app->get('/designs', function () use (&$db, $app) {

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}
		
		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "designs.id";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		$type = isset($_GET['type'])? $_GET['type'] : "null";
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		$first = ($page-1) * $items_per_page;
		$total = isset($_GET['total']) ? $_GET['total'] : 0;
		$with_credits = isset($_GET['with_credits']) ? $_GET['with_credits'] : 'null';
		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$select = "select designs.id, designs.filename, credits.id credit_id, credits.token, credits.type, credits.group_id, designs.width, designs.height ";
		$limit = "limit :first , :items_per_page";

		if($total == 1){
			$select = "select count(1) as total ";
			$limit = "";
		}else if($csv == 1){
			$select = "select designs.id, designs.filename, credits.id credit_id, credits.token, credits.type, credits.group_id, designs.width, designs.height ";
			$limit = "";
		}

		$q = $select.
			    "from 
			    	designs left join credits on designs.credit_id = credits.id
			where 
				(lower(token) like :search or lower(group_id) like :search or lower(data) like :search )
				and ( :type = 'null' or type = :type ) 
				and (
					(:with_credits = '1' and credits.id is not null)
					or
					(:with_credits = '0' and credits.id is null)
					or
					(:with_credits = 'null')
					) 
				order by {$order_by} {$order_how} ".
			$limit;

			
		$query = $db->prepare($q);

		if($total != 1 && $csv != 1) {
			$query->bindValue(':first', (int) $first, PDO::PARAM_INT);
			$query->bindValue(':items_per_page', (int) $items_per_page, PDO::PARAM_INT);
		}
		$query->bindValue(':search', $search, PDO::PARAM_STR );
		$query->bindValue(':type', $type, PDO::PARAM_STR );
		$query->bindValue(':with_credits', $with_credits, PDO::PARAM_STR );


		$query->execute();

		if($total == 1){
			echo json_encode($query->fetch(PDO::FETCH_ASSOC));
		}else if($csv == 1){
			$data = $query->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=designs.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}
		else{
			echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		}
	  
	});


	/** 
	[method] : GET
	[url] : /validate/designs
	*/
	$app->get('/validate/designs', function () use (&$db, $app) {

		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}
		
		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "designs.id";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		$type = isset($_GET['type'])? $_GET['type'] : "null";
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		$first = ($page-1) * $items_per_page;
		$total = isset($_GET['total']) ? $_GET['total'] : 0;
		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$status = isset($_GET['status']) ? $_GET['status'] : 'all';
		$upload = isset($_GET['upload']) ? $_GET['upload'] : 'null';

		if ($total == 1){
			$q = "
			select count(1) as total
			from
			(
				select 
					designs.id, 
					designs.filename, 
					credits.id credit_id, 
					credits.token, 
					credits.type, 
					credits.group_id, 
					designs.width, 
					designs.height,
					case when (designs.data is null or length(designs.data) = 0) then 1 else 0 end upload,
					(select value 
						from validations 
						where 
							design_id = designs.id 
							and validations.timestamp = (select max(validations.timestamp) from validations where design_id = designs.id)) last_validation 
				from 
					designs join credits on designs.credit_id = credits.id
					join orders on credits.order_id = orders.id
				where 
					(lower(token) like :search or lower(group_id) like :search or lower(data) like :search )
					and ( :type = 'null' or type = :type ) 
					and orders.flow <> 'm'
				having
					(
						(:upload = '1' and upload = 1)
						or
						(:upload = '0' and upload = 0)
						or
						(:upload = 'null')
					)
					and
					(
						(:status = 'pending' and last_validation is null)
						or
						(:status = 'accepted' and last_validation = 1)
						or
						(:status = 'rejected' and last_validation = 0)
						or
						(:status = 'all')
					)
			) as tbl";

			$query = $db->prepare($q);

			$query->bindValue(':search', $search, PDO::PARAM_STR );
			$query->bindValue(':type', $type, PDO::PARAM_STR );
			$query->bindValue(':upload', $upload, PDO::PARAM_STR );
			$query->bindValue(':status', $status, PDO::PARAM_STR );
			$query->execute();

			echo json_encode($query->fetch(PDO::FETCH_ASSOC));
		}elseif($csv == 1){
			$q = "
			select 
				designs.id, 
				designs.filename, 
				credits.id credit_id, 
				credits.token, 
				credits.type, 
				credits.group_id, 
				designs.width, 
				designs.height,
				case when (designs.data is null or length(designs.data) = 0) then 1 else 0 end upload,
				(select value 
					from validations 
					where 
						design_id = designs.id 
						and validations.timestamp = (select max(validations.timestamp) from validations where design_id = designs.id)) last_validation 
			from 
				designs join credits on designs.credit_id = credits.id
				join orders on credits.order_id = orders.id
			where 
				(lower(token) like :search or lower(group_id) like :search or lower(data) like :search )
				and ( :type = 'null' or type = :type ) 
				and orders.flow <> 'm'
			having
				(
					(:upload = '1' and upload = 1)
					or
					(:upload = '0' and upload = 0)
					or
					(:upload = 'null')
				)
				and
				(
					(:status = 'pending' and last_validation is null)
					or
					(:status = 'accepted' and last_validation = 1)
					or
					(:status = 'rejected' and last_validation = 0)
					or
					(:status = 'all')
				)
			order by {$order_by} {$order_how}";

			$query = $db->prepare($q);

			$query->bindValue(':search', $search, PDO::PARAM_STR );
			$query->bindValue(':type', $type, PDO::PARAM_STR );
			$query->bindValue(':upload', $upload, PDO::PARAM_STR );
			$query->bindValue(':status', $status, PDO::PARAM_STR );
			$query->execute();

			$data = $query->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=validation_credits.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}else{
			$q="select 
				designs.id, 
				designs.filename, 
				credits.id credit_id, 
				credits.token, 
				credits.type, 
				credits.group_id, 
				designs.width, 
				designs.height,
				case when (designs.data is null or length(designs.data) = 0) then 1 else 0 end upload,
				(select value 
					from validations 
					where 
						design_id = designs.id 
						and validations.timestamp = (select max(validations.timestamp) from validations where design_id = designs.id)) last_validation 
			from 
				designs join credits on designs.credit_id = credits.id
				join orders on credits.order_id = orders.id
			where 
				(lower(token) like :search or lower(group_id) like :search or lower(data) like :search )
				and ( :type = 'null' or type = :type ) 
				and orders.flow <> 'm'
			having
				(
					(:upload = '1' and upload = 1)
					or
					(:upload = '0' and upload = 0)
					or
					(:upload = 'null')
				)
				and
				(
					(:status = 'pending' and last_validation is null)
					or
					(:status = 'accepted' and last_validation = 1)
					or
					(:status = 'rejected' and last_validation = 0)
					or
					(:status = 'all')
				)
			order by {$order_by} {$order_how} limit :first , :items_per_page";

			$query = $db->prepare($q);

			$query->bindValue(':first', (int) $first, PDO::PARAM_INT);
			$query->bindValue(':items_per_page', (int) $items_per_page, PDO::PARAM_INT);
			$query->bindValue(':search', $search, PDO::PARAM_STR );
			$query->bindValue(':type', $type, PDO::PARAM_STR );
			$query->bindValue(':upload', $upload, PDO::PARAM_STR );
			$query->bindValue(':status', $status, PDO::PARAM_STR );
			$query->execute();

			echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		}
	  
	});

/** 
	[method] : GET
	[url] : /validate/designs/random
	*/
$app->get('/validate/designs/random', function () use (&$db, $app) {
	    	$app->log->debug("[GET]/validate/designs/random");

		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select 
				designs.id, 
				designs.filename, 
				credits.id credit_id, 
				credits.token, 
				credits.type, 
				credits.group_id, 
				orders.order_id, orders.transaction_date, orders.cardholder, orders.email, orders.amount, orders.currency,
				designs.width, 
				designs.height,
				(select value 
					from validations 
					where 
						design_id = designs.id 
						and validations.timestamp = (select max(validations.timestamp) from validations where design_id = designs.id)) last_validation
				from 
				designs join credits on designs.credit_id = credits.id
				join orders on credits.order_id = orders.id
				where designs.id not in (select design_id from validations where design_id is not null)
				and orders.flow <> 'm'
				order by rand() limit 1";
		$query = $db->prepare($q);
		$query->execute();
		echo json_encode($query->fetch(PDO::FETCH_ASSOC)); 
});

/** 
	[method] : GET
	[url] : /validate/designs/:id
	*/
$app->get('/validate/designs/:id', function ($id) use (&$db, $app) {
	    	$app->log->debug("[GET]/validate/designs/".$id);

		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select 
				designs.id, 
				designs.filename, 
				credits.id credit_id, 
				credits.token, 
				credits.type, 
				credits.group_id, 
				orders.order_id, orders.transaction_date, orders.cardholder, orders.email, orders.amount, orders.currency,
				designs.width, 
				designs.height,
				(select value 
					from validations 
					where 
						design_id = designs.id 
						and validations.timestamp = (select max(validations.timestamp) from validations where design_id = designs.id)) last_validation
				from 
				designs join credits on designs.credit_id = credits.id
				join orders on credits.order_id = orders.id
				where designs.id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();

		$result = $query->fetch(PDO::FETCH_ASSOC);
		if(empty($result['email'])) {
			$q = "select firstname, lastname, email from neighbours where token = :token";
			$query = $db->prepare($q);
			$query->bindValue(':token', $result['token'], PDO::PARAM_STR);
			$query->execute();
			$neighbour = $query->fetch(PDO::FETCH_ASSOC);
			if($neighbour) {
				$result['cardholder'] = $neighbour['firstname'] . ' ' . $neighbour['lastname'];
				$result['email'] = $neighbour['email'];
			}
		}
		echo json_encode($result); 
});

	/** 
	[method] : DELETE
	[url] : /designs/:id
	*/
	$app->delete('/designs/:id', function ($id) use (&$db, $app) {
    	$app->log->debug("[DELETE]/design/".$id);

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select 1 from designs where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$exists = $query->fetch(PDO::FETCH_ASSOC);
		
		if($exists){
			$app->log->debug("delete from designs where id = ".$id);				
			$q = "delete from designs where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();
		}else{
			$app->log->debug("*ERROR* no design with id ".$id);			
			$app->response->setStatus(500);
		}
		$app->log->debug("");

	});



	/** 
	[method] : POST
	[url] : /designs/:id/validate
	*/
	$app->post('/designs/:id/validate', function ($id) use (&$db, $app) {
		$app->log->debug("[POST]/designs/".$id."/validate");

		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$params = json_decode(file_get_contents('php://input'), true);
		if(!isset($params['validation_code']) || !is_numeric($params['validation_code'])){
			$app->log->debug("*ERROR* no valid validation in post");
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}

		$validation_code = (int) $params['validation_code'];
		if($validation_code < 0 || $validation_code > 1){
			$app->log->debug("*ERROR* validation must be 0 or 1");
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}

		// see if design exists
		$q = "select * from designs where id = :id";
		$app->log->debug("select * from designs where id = " .$id);

		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$design = $query->fetch(PDO::FETCH_ASSOC);

		
		if($design){
			$app->log->debug("design found with id " .$id);

			$user_id = $_SESSION['user']['id'];
			$app->log->debug("user id = " .$user_id);
			
			// see if design is already validated
			$q = "select * from validations where design_id = :design_id";
			$app->log->debug("select * from where validations design_id = ".$id);

			$query = $db->prepare($q);
			$query->bindValue(':design_id', (int) $id, PDO::PARAM_INT);
			$query->execute();
			$validation = $query->fetch(PDO::FETCH_ASSOC);

			if($validation){
				$app->log->debug("design already validated...");
				$app->log->debug("");
				$app->response->setStatus(500);
				echo "This design has alreayd been validated!";
				return;
			}else{
				$app->log->debug("no validation found, creating new one");
				$timestamp = date('Y-m-d H:i:s');
				$app->log->debug("generated timestamp ".$timestamp);
				
				$q = "insert into validations (user_id, design_id, value, timestamp) values (:user_id, :design_id, :validation_code, :timestamp)";
				
				$app->log->debug("insert into validations (user_id, design_id, value, timestamp) values (".$user_id.", ".$id.", ".$validation_code.", ".$timestamp.")");				
				$app->log->debug("");
				$query = $db->prepare($q);
				$query->bindValue(':user_id', (int) $user_id, PDO::PARAM_INT);
				$query->bindValue(':design_id', (int) $id, PDO::PARAM_INT);
				$query->bindValue(':validation_code', (int) $validation_code, PDO::PARAM_INT);
				$query->bindValue(':timestamp', $timestamp, PDO::PARAM_STR);
				$query->execute();

				if($validation_code == "0"){
					//rejected
					handleRejectedDesign($app, $db, $_SESSION['user'], $id);
				}else{
					//accepted
					handleAcceptedDesign($app, $db, $_SESSION['user'], $id);
				}

			}

		}else{
			$app->log->debug("no design found with id " .$id);
			$app->log->debug("");
			$app->response->setStatus(500);
			return;
		}
		

	});


	function handleRejectedDesign($app, $db, $user, $design_id) {
		$app->log->debug("handleRejectedDesign");
		$app->log->debug("  user id: ". $user['id']);
		$app->log->debug("  design id: ". $design_id);
		$app->log->debug("");

		// look up order details

		// create new credit in order

		// send email to order.email with credit.token in it
		$q = "select orders.id, orders.cardholder, orders.email, credits.type, credits.token, credits.group_id from orders join credits on credits.order_id = orders.id join designs on designs.credit_id = credits.id where designs.id = :design_id";
		$query = $db->prepare($q);
		$query->bindValue(':design_id', (int) $design_id, PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);

		$data = array();

		if($result) {
			$order_id = $result['id'];
			$type = $result['type'];
			$group_id = $result['group_id'];
			$token = $result['token'];

			$q = "delete from designs where id = :id";
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $design_id, PDO::PARAM_INT);
			$query->execute();

			// $tries = 100;
			// while($tries > 0){
			// 	$app->log->debug("generating token... attempt #".(101-$tries));
			// 	//generate token
			// 	$random = rand ( 1, 999 );
   //      $token = mb_substr ( (string)sha1 ( $id . "-$random" ), 0, 10 );
   //      $app->log->debug("generated_token=".$token);

   //      // test if token exists
   //      $q = "select 1 from credits where token = :token limit 1";
			// 	$query = $db->prepare($q);
			// 	$query->bindValue(':token', $token, PDO::PARAM_STR);
			// 	$query->execute();
			// 	$exists = $query->fetch(PDO::FETCH_ASSOC);

			// 	if(!$exists){
			// 		$app->log->debug("insert into credits (order_id, token, type, group_id) values (".$order_id.", ".$token.", ".$type.", ".$group_id.")");
			// 		$q = "insert into credits (order_id, token, type, group_id) values (:order_id, :token, :type, :group_id)";
			// 		$query = $db->prepare($q);
			// 		$query->bindValue(':order_id', $order_id, PDO::PARAM_INT);
			// 		$query->bindValue(':token', $token, PDO::PARAM_STR);
			// 		$query->bindValue(':type', $type, PDO::PARAM_STR);
			// 		$query->bindValue(':group_id', $group_id, PDO::PARAM_STR);
			// 		$query->execute();
			// 		break;
			// 	}else {
			// 		$tries -= 1;
			// 	}
			// }
			
			// $app->log->debug("result: " . json_encode($result));

			if(empty($result['email'])) {
				// $q = "select credits.token, neighbours.id from designs join credits on credits.id = designs.credit_id join neighbours on neighbours.token = credits.token where designs.id = :design_id";
				// $query = $db->prepare($q);
				// $query->bindValue(':design_id', (int) $design_id, PDO::PARAM_INT);
				// $query->execute();
				// $info = $query->fetch(PDO::FETCH_ASSOC);
				// if($info) {
				// 	$q = "update neighbours set token = :token where id = :id";
				// 	$query = $db->prepare($q);
				// 	$query->bindValue(':token', $token, PDO::PARAM_STR);
				// 	$query->bindValue(':id', (int) $info['id'], PDO::PARAM_INT);
				// 	$query->execute();
				// }

				$q = "select firstname, lastname, email from neighbours where token = :token";
				$query = $db->prepare($q);
				$query->bindValue(':token', $token, PDO::PARAM_STR);
				$query->execute();
				$neighbour = $query->fetch(PDO::FETCH_ASSOC);
				if($neighbour) {
					$result['cardholder'] = $neighbour['firstname'] . ' ' . $neighbour['lastname'];
					$result['email'] = $neighbour['email'];
				}
			}


			if(isset($result['email'])) {
				$data = array(
					'cardholder' => $result['cardholder'],
					'email' => $result['email'],
					'token' => $token,
					'type' => $type,
					'group_id' => $group_id
				);

				$template = 'mails/reject.php';

				ob_start();
				include $template;
				$email_body = ob_get_contents();
				ob_end_clean();

				// $app->log->debug($email_body);

				$content = array('subject'=> 'Your design | People Of Tomorrow', 'html' => $email_body, 'text' => $email_body);

				$param = array(
					'tag' => array('rejected'), 
					'mailfrom' => 'contact@peopleoftomorrow.com',
					'mailfrom_friendly' => 'People Of Tomorrow', 
					'replyto' => 'contact@peopleoftomorrow.com', 
					'replyto_filtered' => 'true'
				);

				$emails = array(array('email' => $data['email']));
				// $emails = array(array('email' => 'ken@enden.be'));

				$critsend = new MxmConnect();
				$success = $critsend->sendCampaign($content, $param, $emails);

			}

		}
	}


	function handleAcceptedDesign($app, $db, $user, $design_id) {
		$app->log->debug("handleAcceptedDesign");
		$app->log->debug("  user id: ". $user['id']);
		$app->log->debug("  design id: ". $design_id);
		$app->log->debug("");
		

		$q = "select orders.cardholder, orders.email, credits.token, designs.filename from orders join credits on credits.order_id = orders.id join designs on designs.credit_id = credits.id where designs.id = :design_id";
		$query = $db->prepare($q);
		$query->bindValue(':design_id', (int) $design_id, PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);

		print_r($_SERVER);
		$certificate = "http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . '/certificates/' . generateCertificate($app, $db, $user, $design_id);

		$app->log->debug("certificate: ".$certificate);

		if($result) {
			// copy file to local folder
			$file = 'http://www.peopleoftomorrow.com/preview/' . $result['filename'];
			$newfile = $_SERVER['DOCUMENT_ROOT'] . '/designs/' . $result['filename'];

			copy($file, $newfile);
		}

		if(empty($result['email'])) {
			$q = "select firstname, lastname, email from neighbours where token = :token";
			$query = $db->prepare($q);
			$query->bindValue(':token', $result['token'], PDO::PARAM_STR);
			$query->execute();
			$neighbour = $query->fetch(PDO::FETCH_ASSOC);
			if($neighbour) {
				$result['cardholder'] = $neighbour['firstname'] . ' ' . $neighbour['lastname'];
				$result['email'] = $neighbour['email'];
			}
		}

		if ($result && isset($result['email'])) {

			$data = array(
				'cardholder' => $result['cardholder'],
				'email' => $result['email'],
				'certificate' => $certificate
			);

			$template = 'mails/accept.php';

			ob_start();
			include $template;
			$email_body = ob_get_contents();
			ob_end_clean();

			// $app->log->debug($email_body);

			$content = array('subject'=> 'Congratulations | People Of Tomorrow', 'html' => $email_body, 'text' => $email_body);

			$param = array(
				'tag' => array('accepted'), 
				'mailfrom' => 'contact@peopleoftomorrow.com',
				'mailfrom_friendly' => 'People Of Tomorrow', 
				'replyto' => 'contact@peopleoftomorrow.com', 
				'replyto_filtered' => 'true'
			);

			$emails = array(array('email' => $data['email']));
			// $emails = array(array('email' => 'ken@enden.be'));

			$critsend = new MxmConnect();
			$success = $critsend->sendCampaign($content, $param, $emails);
		}
		
	}

	function generateCertificate($app, $db, $user, $design_id) {
		$app->log->debug("generateCertificate");
		$app->log->debug("  design id: ". $design_id);
		$app->log->debug("");

		$q = "select type, width, height, filename from designs join credits on designs.credit_id = credits.id where designs.id = :design_id";
		$query = $db->prepare($q);
		$query->bindValue(':design_id', (int) $design_id, PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);

		if($result) {
			$filename = $result['filename'];
			$image = 'http://www.peopleoftomorrow.com/preview/' . $filename;
			$template = "pdf/certificaat-square.pdf";
			$x = 117.1;
			$y = 56.0;
			$w = 62.1;
			$h = 62.1;

			$type = $result['type'];
			if($type == 'large') {
				$template = "pdf/certificaat-large.pdf";
				$x = 49.3;
				$y = 59.5;
				$w = 201.8;
				$h = 49.2;
			} else if($type == 'normal' && $result['width'] != 693) {
				$template = "pdf/certificaat-rectangle.pdf";
				$x = 65.3;
				$y = 63.9;
				$w = 166.0;
				$h = 39.6;
			}

			$app->log->debug("image: " . $image);
			$app->log->debug("template: " . $template);
			$app->log->debug("type: " . $type);

			$pdf = new FPDI();
			$pdf->AddPage();
			$pdf->setSourceFile($template);
			$tplIdx = $pdf->importPage(1);
			$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
			$pdf->Image($image, $x, $y, $w, $h, 'PNG');



			$certificate = substr_replace($filename, 'pdf', strrpos($filename , '.')+1);

			$pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/certificates/' . $certificate, "F");

			return $certificate;
		} else {
			return false;
		}

	}

/** 
	[method] : POST
	[url] : /designs/:id/certificate
	*/
	$app->post('/designs/:id/certificate', function ($id) use (&$db, $app) {
		$app->log->debug("[POST]/designs/".$id."/validate");

		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select filename from designs where designs.id = :design_id";
		$query = $db->prepare($q);
		$query->bindValue(':design_id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);

		if($result) {
			// copy file to local folder
			$file = 'http://www.peopleoftomorrow.com/preview/' . $result['filename'];
			$newfile = $_SERVER['DOCUMENT_ROOT'] . '/designs/' . $result['filename'];

			copy($file, $newfile);
		}

		echo "http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . '/certificates/' . generateCertificate($app, $db, $_SESSION['user'], $id);
		return;

	});

/** 
	[method] : GET
	[url] : /designs/:id/validate
	*/
	$app->get('/designs/:id/validate', function ($id) use (&$db, $app) {
		$app->log->debug("[GET]/designs/".$id."/validate");

		if(!allowed(array("admin", "validator"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select users.id user_id, users.username, users.email, validations.timestamp, validations.value validation, designs.id design_id, designs.filename
				from validations 
					join users on validations.user_id = users.id
					join designs on validations.design_id = designs.id
				where
					designs.id = :id
				order by timestamp desc";

		$query = $db->prepare($q);
		$query->bindValue(':id', $id, PDO::PARAM_INT );
		$query->execute();
		echo json_encode($query->fetchAll(PDO::FETCH_ASSOC)); 

	});

/** 
	[method] : POST
	[url] : /designs/validate-manual
	*/
	$app->post('/designs/validate-manual', function() use (&$db, $app) {
		$app->log->debug("[POST]/designs/validate-manual");

		if(!allowed(array("admin"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		// select all designs where orders.flow is 'm' and design is not validated
		$q = "select designs.id, designs.filename
			from orders join credits on credits.order_id = orders.id join designs on designs.credit_id = credits.id
			where orders.flow = 'm' and not exists (select 1 from validations where validations.design_id = designs.id)";
		$query = $db->prepare($q);
		$query->execute();
		$designs = $query->fetchAll(PDO::FETCH_ASSOC);
		// set validation to 1
		$user_id = $_SESSION['user']['id'];
		$validation_code = 1;
		$timestamp = date('Y-m-d H:i:s');
		foreach ($designs as $design) {
			// copy file to local folder
			$file = 'http://www.peopleoftomorrow.com/preview/' . $design['filename'];
			$newfile = $_SERVER['DOCUMENT_ROOT'] . '/designs/' . $design['filename'];

			copy($file, $newfile);
			$id = $design['id'];
			$q = "insert into validations (user_id, design_id, value, timestamp) values (:user_id, :design_id, :validation_code, :timestamp)";
			$query = $db->prepare($q);
			$query->bindValue(':user_id', (int) $user_id, PDO::PARAM_INT);
			$query->bindValue(':design_id', (int) $id, PDO::PARAM_INT);
			$query->bindValue(':validation_code', (int) $validation_code, PDO::PARAM_INT);
			$query->bindValue(':timestamp', $timestamp, PDO::PARAM_STR);
			$query->execute();
		}
	});

/** 
	[method] : POST
	[url] : /designs/remove-validate-manual
	*/
	$app->post('/designs/remove-validate-manual', function() use (&$db, $app) {
		$app->log->debug("[POST]/designs/remove-validate-manual");

		if(!allowed(array("admin"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "delete from validations where design_id in (select designs.id
			from orders join credits on credits.order_id = orders.id join designs on designs.credit_id = credits.id
			where orders.flow = 'm')";
		$query = $db->prepare($q);
		$query->execute();
	});
?>
