<?php

	/** 
	[method] : GET
	[url] : /neighbours
	*/
	$app->get('/neighbours', function () use (&$db, $app) {

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$search = isset($_GET['search'])? "%".strtolower($_GET['search'])."%" : "%%";
		$order_by = isset($_GET['sort_by'])? $_GET['sort_by'] : "neighbours.id";
		$order_how = isset($_GET['sort_how'])? $_GET['sort_how'] : "desc";
		// $type = isset($_GET['used'])? $_GET['used'] : "null";
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$items_per_page = isset($_GET['items_per_page']) ? $_GET['items_per_page'] : 100;
		$first = ($page-1) * $items_per_page;
		$total = isset($_GET['total']) ? $_GET['total'] : 0;
		$csv = isset($_GET['csv']) ? $_GET['csv'] : 0;

		$search = str_replace(",", "%", str_replace(" ", "%", $search));


		$q = "";

		if($csv == 1){
			$q = "select neighbours.*, designs.id design_id, designs.credit_id credit_id, designs.filename filename 
						from neighbours
								left join credits on neighbours.token = credits.token 
								left join designs on designs.credit_id = credits.id 
						where 
							neighbours.used = 1
							and lower(concat(neighbours.token, ' ', neighbours.firstname, ' ', neighbours.lastname, ' ', neighbours.email, ' ', neighbours.street, ' ', neighbours.streetnr, ' ', neighbours.city, ' ', neighbours.zip, ' ', neighbours.country)) like :search
						order by {$order_by} {$order_how} ";
		}
		else if($total == 1){ 
			$q = "select count(1) as total 
			from neighbours
								left join credits on neighbours.token = credits.token 
								left join designs on designs.credit_id = credits.id 
						where 
							neighbours.used = 1
							and lower(concat(neighbours.token, ' ', neighbours.firstname, ' ', neighbours.lastname, ' ', neighbours.email, ' ', neighbours.street, ' ', neighbours.streetnr, ' ', neighbours.city, ' ', neighbours.zip, ' ', neighbours.country)) like :search";

		}else {
			$q = "select neighbours.*, designs.id design_id, designs.credit_id credit_id, designs.filename filename 
			    from neighbours
					left join credits on neighbours.token = credits.token 
					left join designs on designs.credit_id = credits.id 
			where 
				neighbours.used = 1
				and lower(concat(neighbours.token, ' ', neighbours.firstname, ' ', neighbours.lastname, ' ', neighbours.email, ' ', neighbours.street, ' ', neighbours.streetnr, ' ', neighbours.city, ' ', neighbours.zip, ' ', neighbours.country)) like :search
			order by {$order_by} {$order_how} 
			limit :first , :items_per_page";
		}

		

		// explore($q);
		// explore($_GET);

		// print(time());
			
		$query = $db->prepare($q);
		
		if($total != 1 && $csv != 1) {
			$query->bindValue(':first', (int) $first, PDO::PARAM_INT);
			$query->bindValue(':items_per_page', (int) $items_per_page, PDO::PARAM_INT);
		}

		$query->bindValue(':search', $search, PDO::PARAM_STR );
		

		$query->execute();

		if($csv == 1){
			$data = $query->fetchAll(PDO::FETCH_ASSOC);
			if(sizeof($data) > 0){
				$app->response->headers->set('Content-Type', 'application/octet-stream');
				$app->response->headers->set("Content-Disposition", "attachment; filename=neighbours.csv");
				$app->response->headers->set("Pragma", "no-cache");
				$app->response->headers->set("Expires", "0");				
				//titles
				echo implode(",", array_map("quote", array_values(array_keys($data[0])))) . PHP_EOL;
				//data
				foreach ($data as $line){
					echo implode(",",array_map("quote", array_values($line))) . PHP_EOL;
				}
			}
		}
		else if($total == 1){
			echo json_encode($query->fetch(PDO::FETCH_ASSOC));
		}else{
			echo json_encode($query->fetchAll(PDO::FETCH_ASSOC));
		}
	  
	});


	$app->put('/neighbours/:id', function ($id) use (&$db, $app) {
		$app->log->debug("[PUT]neighbours/".$id);

		if(!allowed(array("admin", "helpdesk"))){
			$app->log->debug("*ERROR* NOT ALLOWED -- STATUS 401");
			$app->log->debug("");
			$app->response->setStatus(401);
			return;
		}

		$q = "select * from neighbours where id = :id";
		$query = $db->prepare($q);
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();
		$neighbour = $query->fetch(PDO::FETCH_ASSOC);

		if($neighbour){
			$app->log->debug("neighbour with ".$id. " exists");
			$token = $neighbour['token'];
			
			if(!$token){
				$app->log->debug("*ERROR* no token in neighbour with id ".$id);
				$app->log->debug("");
				$app->response->setStatus(500);	
				return;
			}

			$app->log->debug("neighbour token ".$token);

			$app->log->debug("select * from credits where token = ".$token);

			$q = "select * from credits where token = :token";
			$query = $db->prepare($q);
			$query->bindValue(':token', $token, PDO::PARAM_STR);
			$query->execute();
			$credits = $query->fetchAll(PDO::FETCH_ASSOC);

			$app->log->debug("found ". sizeof($credits) ." credits");
			foreach($credits as $credit){
				$credit_id = $credit['id'];
				$app->log->debug("delete from designs where credit_id = ".$credit['id']);
				$q = "delete from designs where credit_id = :credit_id";
				$query = $db->prepare($q);
				$query->bindValue(':credit_id', (int) $credit_id, PDO::PARAM_INT);
				$query->execute();

			}

			$app->log->debug("resetting neighbour with id ". $id);

			$q = "update neighbours 
				set used = 0, email = '', firstname = '', lastname = '', street = '', streetnr = '', city = '', zip = '', country = ''
				where id = :id";
			
			$query = $db->prepare($q);
			$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
			$query->execute();
			$app->log->debug("");

		}else{
			$app->log->debug("*ERROR* no neighbour with id ".$id);
			$app->log->debug("");
			$app->response->setStatus(500);		
		}

	});

	/**
	[method] : POST
	[url] : /neighbours/:id/email
	*/

	$app->post('/neighbours/:id/email', function($id) use (&$db, $app) {
		$app->log->debug("[POST]/neighbours/".$id."/email");
		// order cardholder, order email
		$query = $db->prepare("select firstname, lastname, email, token from neighbours where neighbours.id = :id");
		$query->bindValue(':id', (int) $id, PDO::PARAM_INT);
		$query->execute();

		$neighbour = $query->fetch(PDO::FETCH_ASSOC);

		$email = isset($neighbour['email']) ? trim($neighbour['email']) : null;
		
		if(!$email || empty($email)) {
			$app->log->debug("*ERROR* no email found for neighbour ".$id);		
			$app->log->debug("");
			$app->response->setStatus(500);
			echo "No email found for neighbour ".$id;
			return;
		}
		
		// data for template
		$data = array(
			'name' => trim($neighbour['firstname'] . ' ' . $neighbour['lastname']),
			'token' => $neighbour['token']
		);

		$template = 'mails/neighbour.php';

		ob_start();
		include $template;
		$email_body = ob_get_contents();
		ob_end_clean();

		// $app->log->debug($email_body);

		$content = array('subject'=> 'Uw code | People Of Tomorrow', 'html' => $email_body, 'text' => $email_body);

		$param = array(
			'tag' => array('neighbour'), 
			'mailfrom' => 'contact@peopleoftomorrow.com',
			'mailfrom_friendly' => 'People Of Tomorrow', 
			'replyto' => 'contact@peopleoftomorrow.com', 
			'replyto_filtered' => 'true'
		);

		$emails = array(array('email' => $email));

		$critsend = new MxmConnect();
		$success = $critsend->sendCampaign($content, $param, $emails);
		echo json_encode(array('success' => $success));

	});
?>
