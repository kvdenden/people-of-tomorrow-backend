<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" name="viewport" />

	<title>Your design | People of Tomorrow</title>
</head>

<body>
	<p>&nbsp;</p>

	<table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
		<tr>
			<td style="background-color: #f4f4f4">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="620">
					<tr>
						<td height="66" style="line-height: 0;" valign="bottom"><img height="56" src="http://www.peopleoftomorrow.com/images/mails/pot.png" width="98" /></td>

						<td align="right" style="" valign="bottom"><img height="35" src="http://www.peopleoftomorrow.com/images/mails/arne.png" width="273" /></td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
					<tr>
						<td style="line-height: 10px; font-size: 10px;">&nbsp;</td>
					</tr>

					<tr>
						<td style="line-height: 0;"><img height="3" src="http://www.peopleoftomorrow.com/images/mails/line.png" width="640" /></td>
					</tr>

					<tr>
						<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="590">
					<tr>
						<td style="font-family: helvetica; color: #676767; font-size: 14px; line-height: 20px;">
							<span style="font-weight: bold;"><p>HELLO <span style="color: #30c0fc;"><?php echo strtoupper($data['cardholder']); ?></span><br /></p></span>

								<span style="font-size: 12px; font-weight: normal; line-height: 18px;"><p>Congratulations!</p>

								<p>Your message has been validated</p>

							<p>You have created an essential building block of the One World Tomorrowland Bridge by the People of Tomorrow. As the bridge approaches completion, you will receive more information from us on how to locate your message within the bridge.</p>
							
							<p>Click <a href="<?php echo $data['certificate']; ?>">here</a> to download your certificate.<br />
							Shout out your message to the World!</p></span>

						</td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="590">
					<tr>
						<td>
							<table align="right" border="0" cellpadding="0" cellspacing="0" width="400">
								<tr>
									<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
								</tr>

								<tr>
									<td>
										<table align="right" border="0" cellpadding="0" cellspacing="0" width="400"></table>
									</td>
								</tr>

								<tr>
									<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
								</tr>
							</table>
						</td>

						<td width="75">&nbsp;</td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="590">
					<tr>
						<td style="font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;">
							
							<p><br />For further questions, requests or feedback please visit <a href="http://www.peopleoftomorrow.com/en/contact" style="color: #30c0fc;">www.peopleoftomorrow.com</a></p>

							<p>Thank you,</p>

							<p>Don't forget to become part of People of Tomorrow on<br />
							<a href="http://www.facebook.com/Peopleoftomorrowofficial" style="color: #676767;">facebook</a>, <a href="https://twitter.com/POTomorrow" style="color: #676767;">twitter</a> or <a href="http://instagram.com/peopleoftomorrowofficial#" style="color: #676767;">instagram</a> !</p>
						</td>
					</tr>

					<tr>
						<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>