<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" name="viewport" />

	<title>Your credits | People of Tomorrow</title>
</head>

<body>
	<p>&nbsp;</p>

	<table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
		<tr>
			<td style="background-color: #f4f4f4">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="620">
					<tr>
						<td height="66" style="line-height: 0;" valign="bottom"><img height="56" src="http://www.peopleoftomorrow.com/images/mails/pot.png" width="98" /></td>

						<td align="right" style="" valign="bottom"><img height="35" src="http://www.peopleoftomorrow.com/images/mails/arne.png" width="273" /></td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="640">
					<tr>
						<td style="line-height: 10px; font-size: 10px;">&nbsp;</td>
					</tr>

					<tr>
						<td style="line-height: 0;"><img height="3" src="http://www.peopleoftomorrow.com/images/mails/line.png" width="640" /></td>
					</tr>

					<tr>
						<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="590">
					<tr>
						<td style="font-family: helvetica; font-weight: bold; color: #676767; font-size: 14px; line-height: 20px;">
							<p>HELLO <span style="color: #30c0fc;"><?php echo strtoupper($data['cardholder']); ?></span><br /></p>

							<p style="font-size: 12px; font-weight: normal; line-height: 18px;">This mail contains your unique and exclusive credit code(s) to enter in our online editor and <a href="http://www.peopleoftomorrow.com/en/participate/create" target="_blank" style="color: #30c0fc; font-weight: bold;">create your message</a>!</p>
						</td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="590">
					<tr>
						<td>
							<table align="right" border="0" cellpadding="0" cellspacing="0" width="400">
								<tr>
									<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
								</tr>

								<tr>
									<td>
										<table align="right" border="0" cellpadding="0" cellspacing="0" width="400">
											<tr>
												<td style="border-bottom: 2px solid #676767; font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;" valign="top">ORDER INFORMATION</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td>
										<table align="right" border="0" cellpadding="0" cellspacing="0" width="400">
											<?php foreach($data['credits'] as $type => $credits) { ?>
												<?php if(count($credits) > 0) { ?>
													<tr>
														<td height="50" style="font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;" valign="middle" width="75"><?php echo strtoupper($type); ?></td>

														<td style="font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;" valign="middle" width="125"><?php if($type=='group'){ echo 'GROUP ID'; } ?></td>
														<td style="font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;" valign="middle" width="100">CODES</td>
													</tr>
													<?php foreach($credits as $credit) { ?>
														<tr>
															<td style="font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;" valign="top">
																<p style="margin: 0; margin-top: 0;"></p>
															</td>
															<td style="font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;" valign="top">
																<p style="margin: 0; margin-top: 0;"><?php echo $credit['group_id']; ?></p>
															</td>

															<td style="font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;" valign="top"><span style="color: #30c0fc; font-weight: bold;"><?php echo $credit['code']; ?></span></td>
														</tr>
													<?php } ?>
													<tr></tr>
												<?php } ?>
											<?php } ?>
										</table>
									</td>
								</tr>

								<tr>
									<td>
										<table align="right" border="0" cellpadding="0" cellspacing="0" width="400"></table>
									</td>
								</tr>

								<tr>
									<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
								</tr>
							</table>
						</td>

						<td width="75">&nbsp;</td>
					</tr>
				</table>

				<table align="center" border="0" cellpadding="0" cellspacing="0" width="590">
					<tr>
						<td style="font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;">
							<p><br />
							You can copy-paste your code into the validation field to validate your message.<br /></p>

							<p>Thank you,</p>

							<p>Don't forget to become part of People of Tomorrow on<br />
							<a href="http://www.facebook.com/Peopleoftomorrowofficial" style="color: #676767;">facebook</a>, <a href="https://twitter.com/POTomorrow" style="color: #676767;">twitter</a> or <a href="http://instagram.com/peopleoftomorrowofficial#" style="color: #676767;">instagram</a> !</p>
						</td>
					</tr>

					<tr>
						<td style="line-height: 30px; font-size: 30px;">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>